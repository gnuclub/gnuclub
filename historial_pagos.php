<?php
/*
 * historial_pagos.php
 *
 * Copyright 2015 abel <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


require("motor.php");
require("config.php");
?>

<!DOCTYPE html>
<html lang="es">

<head>
	<title>GNUClub/historial de pagos</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="Geany 1.24" />
    <link rel="stylesheet" href="estilo.css">
</head>

<body>

<?php
    $WEB->mainMenu();
    $club_ = $CLUB;
    $id = $_POST["ids"];
    if(!$id) {
        $dni = $_POST["dni"];
        $link = $FUNCIONES->conectar("club");
        $sql = "select id from socio where dni=$dni";
        $res = $link->query($sql);
        $row = $res->fetch_assoc();
        $id = $row["id"];
        $link->close();
    }
?>

<table width="90%"  align="left" id="tab_body">
    <tr><td align="center" valign="top">

    <?php
    echo "<h1>Historial de pagos de ";
    $Socio->getFld("nom", $id);
    echo " ";
    $Socio->getFld("ape", $id);
    echo "</h1>";

    $sql0 = "select fecha, tipo, valor, al_dia, atraso from cuota where ids=$id and valor > 0";
    $l = $FUNCIONES->conectar("club");
    $res = $l->query($sql0);

    echo "<table width='80%' align='center' border='1' id=\"tabla_form\">";
    echo "<tr>
    <td align='center'> <b>Fecha</b> </td>
    <td align='center'> <b>Tipo</b> </td>
    <td align='center'> <b>Monto</b> </td>
    <td align='center'> <b>Al d&iacute;a?</b> </td>
    <td align='center'> <b>Atraso</b> </td>
    </tr>";
    $tipo = "Indefinido";

    while ($row = $res->fetch_array()) {
        echo "<tr id=\"line_list\">";
        echo "<td align='center'>", $row[0], "</td>";

        if($row[1] == 1)  { $tipo = "Mensual"; }
        if($row[1] == 2)  { $tipo = "Bimestral"; }
        if($row[1] == 3)  { $tipo = "Trimestral"; }
        if($row[1] == 4)  { $tipo = "Cuatrimestral"; }
        if($row[1] == 6)  { $tipo = "Semestral"; }
        if($row[1] == 12) { $tipo = "Anual"; }

        echo "<td align='center'>", $tipo, "</td>";
        echo "<td align='center'> $", $row[2], "</td>";
        echo "<td align='center'>", $row[3], "</td>";
        echo "<td align='center'>", $row[4], "</td>";

        echo "</tr>";
    }

    $res->free();
    $l->close();

    echo "</table>";
    ?>

    </td></tr>
</table>
</body>
</html>
