<?php
/*
 * socio_ver.php
 *
 * Copyright 2015 Abel Sendón <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

?>

<?php require("motor.php") ?>
<!DOCTYPE html>
<html>
<head>
    <title>GNUClub/Socio/Ver</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="generator" content="gvim 7.3" />
    <link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" href="estiloSocio.css">
</head>
<body>

<?php
    $WEB->mainMenu();
    // El id de socio se obtiene cuando se viene desde la presentacion de listado
    // parcial (socio_lst.php)
    //
    $id = $_GET["fbd39ff8da6d47fe19e560b268815112a6d47fe19ezrf"];
?>

<table width="90%" align="left" id="tab_body">
<tr><td valign="top" align="center">

<h1>Socio/Ver</h1>

<?php
if(!$id) {
    echo "<table width=\"60%\">";
    echo "<tr>";

    echo "<td align=\"right\">
          <form action=\"socio_ver.php\" method=\"POST\">
          <label for=\"ids\">ID de socio</label></td>
          <td><input type=\"text\" id=\"ids\" name=\"ids\">
          <input type=\"submit\" value=\"Ver\"></td>";

    echo "</tr>";
    echo "<tr>";

    echo "<td align=\"right\">
         <form action=\"socio_ver.php\" method=\"POST\">
         <label for=\"dni\">Nº de c&eacute;dula/DNI de socio</label></td>
         <td><input type=\"text\" id=\"dni\" name=\"dni\">
         <input type=\"submit\" value=\"Ver\"></td>";

    echo "</table>";
}
?>

</form>

<?php
// Si se abre esta pagina directamente se obtiene el id de socio (ids) desde aquí
// en caso de ser consultado a traves del dni de socio se hara una consulta adicional...
//
$ids = $_POST["ids"];
if(!$ids) {
    $ids = $id;
    if(!$ids) {
        $dni = $_POST["dni"];
        $link = $FUNCIONES->conectar("club");
        $res = $link->query("select id from socio where dni = '$dni'");
        $row = $res->fetch_assoc();
        $ids = $row["id"];
        $link->close();
    }
}

if ($ids) {
    echo "<br>";
    $Socio->ver($ids);
    echo "<br><br>";

    // La variable $l define el link hacia las paginas de borrado de socio
    // el hash fbd39... será la variable get en la siguiente pantalla con el valor $ids
    // que define el id de socio.
    //
    $l="socio_del.php?fbd39ff8da6d47fe19e560b268815112a6d47fe19ezrf=$ids&8141807c2aaf8b734619e603d042780a07c2aaf8b734=ver";

    echo "<a href=\"socio_mod.php?id=$ids\"><input type=\"button\" value=\"Modificar\"></a>";
    echo "&nbsp;&nbsp;&nbsp;<a href=\"$l\"><input type=\"button\" value=\" Eliminar \"></a><br><br>";
    echo "<a href=\"socio_dwn.php?id=$ids\"><input type=\"button\" value=\"Dar de baja\"></a>";
    $WEB->nbsp(3);
    echo "<a href=\"socio_udw.php?id=$ids\"><input type=\"button\" value=\"Quitar baja\"></a>";

    echo "<br><br><b>Tipo de plan:</b> ";
    if ($Socio->getFld("tipo", $ids) == -2) {
        echo "<br><a href=\"plan_ver.php?fbd39ff8da6d47fe19e560b268815112a6d47fe19ezrf8b734619e603d042780a07c=$ids\">Modificar tipo de plan</a>";
    }
}

?>

</td></tr>
</table>

</body>
</html>
