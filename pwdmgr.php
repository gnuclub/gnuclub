<?php
/*
 * pwdmgr.php
 *
 * Copyright 2015 abel <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
?>

<!DOCTYPE html>

<?php
// Las siguientes lineas son solo para depurar.
 ini_set('display_errors', TRUE);
// ini_set('display_startup_errors', TRUE);

require("motor.php");
require("config.php");
?>

<head>
	<title>Administrador de base de datos de GNU/Club</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8">
	<meta name="generator" content="gvim 7.3">
    <link rel="stylesheet" href="estilo.css">
</head>

<body>
    <h1>Administrador de contrase&ntilde;as de GNU/Club </h1>
    <?php echo file_get_contents("mnu_admin.html")  ?>

    <h3>Contrase&ntilde;a de administrador</h3>
    <form action="pwdmgr.php" method="POST">
       <input type="hidden" name="enviado" id="enviado" value="1">
       <table width="60%">
            <tr>
                <td><label for="new_pwd_1">Nueva contrase&ntilde;a</label></td>
                <td><input type="password" name="new_pwd_1" id="new_pwd_1"></td>
            </tr>
            <tr>
                <td><label for="new_pwd_2">Confirmar nueva contrase&ntilde;a</label></td>
                <td><input type="password" name="new_pwd_2" id="new_pwd_2"></td>
            </tr>
            <tr>
                <td><label for="old_pwd">Contrase&ntilde;a actual</label></td>
                <td><input type="password" name="old_pwd" id="old_pwd"></td>
            </tr>
            <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
            <tr>
                <td align="right"><input type="submit" value="Modificar"></td>
                <td><input type="reset" value="Reestablecer"></td>
            </tr>
        </table>
    </form>

    <hr>

    <h3>Contrase&ntilde;a de mysql</h3>
    <h4>Debe haber modificado la contrase&ntilde;a de mysql
    previamente.</h4>
    <form action="pwdmgr.php" method="POST">
       <input type="hidden" name="enviado" id="enviado" value="2">
       <table width="60%">
            <tr>
                <td><label for="new_pwd_1">Nueva contrase&ntilde;a</label></td>
                <td><input type="password" name="new_pwd_1" id="new_pwd_1"></td>
            </tr>
            <tr>
                <td><label for="new_pwd_2">Confirmar nueva contrase&ntilde;a</label></td>
                <td><input type="password" name="new_pwd_2" id="new_pwd_2"></td>
            </tr>
            <tr>
                <td><label for="pwd">Contrase&ntilde;a de
                    administrador</label></td>
                <td><input type="password" name="pwd" id="pwd"></td>
            </tr>
            <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
            <tr>
                <td align="right"><input type="submit" value="Modificar"></td>
                <td><input type="reset" value="Reestablecer"></td>
            </tr>
        </table>
    </form>

    <hr>

<?php
$envio = $UTILS->clean($_POST["enviado"]);

if ($envio == 1) {
    $opwd = $UTILS->clean($_POST["old_pwd"]);

    if ($opwd == $ADMIN_PASSWORD) {
        $npwd1 = $UTILS->clean($_POST["new_pwd_1"]);
        $npwd2 = $UTILS->clean($_POST["new_pwd_2"]);

        if ($npwd1 == $npwd2) {
                $fd = fopen("config.php", "w");
                fwrite($fd, "<?php", 128);
                fwrite($fd, "\n//  Archivo autogenerado por GNU Club, los cambios hechos", 128);
                fwrite($fd, "\n//        aqui seran sobreescritos autom�ticamente.\n\n", 128);
                fwrite($fd, "\n//**** Info de la institucion ******************************", 128);
                fwrite($fd, "\n\$CLUB=\"$CLUB\";", 128);
                fwrite($fd, "\n\$CUIT=\"$CUIT\";", 128);
                fwrite($fd, "\n\$DOMICILIO=\"$DOMICILIO\";", 128);
                fwrite($fd, "\n\$TELEFONO=\"$TELEFONO\";", 128);
                fwrite($fd, "\n\n", 128);
                fwrite($fd, "\n//**** Contrase�a de root de mysql *************************", 128);
                fwrite($fd, "\n\$DATABASE_PASSWORD=\"$DATABASE_PASSWORD\";", 128);
                fwrite($fd, "\n\n", 128);
                fwrite($fd, "\n//**** Contrase�a de administrador de gnu/club *************", 128);
                fwrite($fd, "\n\$ADMIN_PASSWORD=\"$npwd1\";", 128);
                fclose($fd);

                echo "<h2>La contrase&ntilde;a de administrador fue modificada exitosamente</h2>";
        }

        else {
               echo "<h2 style=\"color:red\">";
               echo "No hay coincidencia en la nueva contrase&ntilde;a de administrador";
        } // igualdad de nuevo password.
    }

    else {
               echo "<h2 style=\"color:red\">Contrase&ntilde;a de administrador incorrecta";

    } // chequeo de password.
}


else {
    $pwd = $UTILS->clean($_POST["pwd"]);

    if ($pwd == $ADMIN_PASSWORD) {
        $npwd1 = $UTILS->clean($_POST["new_pwd_1"]);
        $npwd2 = $UTILS->clean($_POST["new_pwd_2"]);

        if ($npwd1 == $npwd2) {
                $fd = fopen("config.php", "w");
                fwrite($fd, "<?php", 128);
                fwrite($fd, "\n//  Archivo autogenerado por GNU Club, los cambios hechos", 128);
                fwrite($fd, "\n//        aqui seran sobreescritos autom�ticamente.\n\n", 128);
                fwrite($fd, "\n//**** Info de la institucion ******************************", 128);
                fwrite($fd, "\n\$CLUB=\"$CLUB\";", 128);
                fwrite($fd, "\n\$CUIT=\"$CUIT\";", 128);
                fwrite($fd, "\n\$DOMICILIO=\"$DOMICILIO\";", 128);
                fwrite($fd, "\n\$TELEFONO=\"$TELEFONO\";", 128);
                fwrite($fd, "\n\n", 128);
                fwrite($fd, "\n//**** Contrase�a de root de mysql *************************", 128);
                fwrite($fd, "\n\$DATABASE_PASSWORD=\"$npwd1\";", 128);
                fwrite($fd, "\n\n", 128);
                fwrite($fd, "\n//**** Contrase�a de administrador de gnu/club *************", 128);
                fwrite($fd, "\n\$ADMIN_PASSWORD=\"$ADMIN_PASSWORD\";", 128);
                fclose($fd);

                echo "<h2>La contrase&ntilde;a de MySQL fue modificada exitosamente</h2>";
        }

        else {
               echo "<h2 style=\"color:red\">";
               echo "No hay coincidencia en la nueva contrase&ntilde;a de MySQL";
        } // igualdad de nuevo password de mysql.
    }

    else {
               echo "<h2 style=\"color:red\">Contrase&ntilde;a de administrador incorrecta";

    } // chequeo de password.

}

?>

</body>
</html>
