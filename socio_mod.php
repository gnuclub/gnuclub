<?php
/*
 * socio_mod.php
 *
 * Copyright 2015 Abel Sendón <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

require("motor.php");
?>


<!DOCTYPE html>
<html lang="es">

<head>
    <title>Club Admin/Socio/Modificar</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="generator" content="gvim 7.3" />
    <link rel="stylesheet" href="estilo.css">
</head>

<body>

<?php
$WEB->mainMenu();
$i =  $_GET["id"];
if(! $i) { $i = $_GET["fbd39ff8da6d47fe19e560b268815112a6d47fe19ezrf"]; }
?>

<table width="90%" align="left" id="tab_body">
<tr><td valign="top" align="center">

    <h1>Socio/Modificar</h1>
    <a href="socio.php"><input type="button" value="Volver al menú socio"></a>
    <br>
    <br>
    <form action="socio__mod.php" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="confirmado" value="1">
    <input type="hidden" name="idr" value="<?php echo $i ?>">
    <?php
        echo "<br>Id de socio: $i<br>";
        echo "<table id=\"tabla_form\">";

    ?>
    <tr>
        <td><label for="nombre">Nombre</label></td>
        <td><input type="text" name="nombre" id="nombre" value="<?php $Socio->getFld("nom", $i) ?>"></td>
    </tr>
    <tr>
        <td><label for="apellido">Apellido</label></td>
        <td><input type="text" name="apellido" id="apellido" value="<?php $Socio->getFld("ape", $i) ?>"></td>
    </tr>
    <tr>
        <td><label for="nacionalidad">Nacionalidad</label></td>
        <td><input type="text" name="nacionalidad" id="nacionalidad" value="<?php $Socio->getFld("nc2", $i) ?>"></td>
    </tr>
    <tr>
        <td><label for="dni">Cédula/DNI</label></td>
        <td><input type="text" name="dni" id="dni" value="<?php $Socio->getFld("dni", $i) ?>"></td>
    </tr>
    <tr>
        <td><label for="estado_civil">Estado civil (<?php $Socio->getFld("est", $i) ?>)</label></td>
        <td><select name="estado_civil" id="estado_civil">
            <option option"soltero/a">Soltero/a</option>
            <option value"casado/a">Casado/a</option>
            <option value"divorciado/a">Divorciado/a</option>
            <option value"viudo/a">Viudo/a</option >
            <option value"concubinato">Concubinato</option >
            </select>
        </td>
    </tr>
    <tr>
        <td><label for="telefono1">Teléfono de casa</label></td>
        <td><input type="text" name="telefono1" id="telefono1" value="<?php $Socio->getFld("te1", $i) ?>"></td>
    </tr>
    <tr>
        <td><label for="telefono2">Teléfono de trabajo</label></td>
        <td><input type="text" name="telefono2" id="telefono2" value="<?php $Socio->getFld("te2", $i) ?>"></td>
    </tr>
    <tr>
        <td><label for="email">eMail</label></td>
        <td><input type="text" name="email" id="email" value="<?php $Socio->getFld("eml", $i) ?>"></td>
    </tr>
    <tr>
        <td><label for="cat">Categor&iacute;a</label></td>
        <td>
            <select name="cat" id="cat">
                <?php
                    $sql = "select * from categorias";
                    $l = $FUNCIONES->conectar("club");
                    $res = $l->query($sql);
                    while ($row = $res->fetch_array()) {
                        echo "\n<option value='".$row["id"]."'>";
                        echo $row["nombre"];
                        echo "</option>";
                    }
                    $l->close();
                ?>
            </select>
        </td>
    </tr>

    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>

    <tr>
        <td><label for="dom_calle">Calle</label></td>
        <td><input type="text" name="dom_calle" id="dom_calle" value="<?php $Socio->getFld("cal", $i) ?>"></td>
    </tr>
    <tr>
        <td><label for="dom_numero">Número</label></td>
        <td><input type="text" name="dom_numero" id="dom_numero" value="<?php $Socio->getFld("num", $i) ?>"></td>
    </tr>
    <tr>
        <td><label for="dom_cp">Código postal</label></td>
        <td><input type="text" name="dom_cp" id="dom_cp" value="<?php $Socio->getFld("cp", $i) ?>"></td>
    </tr>
    <tr>
        <td><label for="depto">Departamento</label></td>
        <td><input type="text" name="depto" id="depto" value="<?php $Socio->getFld("dto", $i) ?>"></td>
    </tr>
    <tr>
        <td><label for="dom_localidad">Localidad</label></td>
        <td><input type="text" name="dom_localidad" id="dom_localidad" value="<?php $Socio->getFld("loc", $i) ?>"></td>
    </tr>
    <tr>
        <td><label for="dom_provincia">Provincia</label></td>
        <td><input type="text" name="dom_provincia" id="dom_provincia" value="<?php $Socio->getFld("pro", $i) ?>"></td>
    </tr>

    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>

    <tr>
        <td><label for="obs">Observaciones</label></td>
        <td><input type="text" name="obs" id="obs" value="<?php $Socio->getFld("obs", $i) ?>"></td>
    </tr>
    </tr>
        <tr>
        <td><label for="foto">Seleccionar nueva foto</label></td>
        <td><input type="file" name="foto" id="foto"></td>
    </tr>

    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>

    <tr>
        <td align="right"><input type="submit" value="Modificar"></td>
        <td><input type="reset" value="Resetear"></td>
    </tr>
    </table>
    </form>


</td></tr>
</table>

</body>
</html>
