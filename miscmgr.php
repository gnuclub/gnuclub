<?php
/*
 * admin.php
 *
 * Copyright 2015 abel <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
?>

<!DOCTYPE html>

<?php

// Las siguientes lineas son solo para depurar.
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require("motor.php");
require("config.php");

?>

<head>
	<title>Administrador de base de datos de GNU/Club</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8">
	<meta name="generator" content="Geany 1.24">
    <link rel="stylesheet" href="estilo.css">
</head>

<body>

    <h1>Administrador de GNU/Club </h1>
    <?php echo file_get_contents("mnu_admin.html")  ?>

<h2>Categor&iacute;as de socios</h2>
</body>

<?php
$status_ = $_POST["estado"];

    if($status_ != 1)
    {
        // Comprobamos si hay categorias de socios creadas, si no hay se crean.
        //
        $sql0 = "select * from categorias";
        $lnk = $FUNCIONES->conectar("club");
        $res = $lnk->query($sql0);

        if($res->fetch_array() == NULL)
        {
            ?>
            <h2>No hay categor&iacute;as de socios creadas</h2>
            <form action="miscmgr.php" method="POST">
            <input type="hidden" name="estado" value="1">

            <table width="24%">
                <tr>
                    <td>
                        <label for="nombre">Categor&iacute;a</label>
                    </td>
                    <td>
                        <input type="text" name="nombre" id="nombre">
                    </td>
                </tr>
                    <td>
                        <label for="obs">Observaciones</label>
                    </td>
                    <td>
                        <input type="text" name="obs" id="obs">
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="reset" value="Reestablecer">
                    </td>
                    <td>
                        <input type="submit" value="Agregar categor&iacute;a">
                    </td>
                </tr>
            </table>
            </form>
            <?php
        }

        else
        {
               echo "<h3>Disponibles actualmente</h3>";
               echo "<table width='60%' id='tabla_form'>";
               echo "<tr>";
               echo "<td><b>Nombre</b></td>";
               echo "<td><b>Observaciones</b></td>";
               echo "<td><b>Acciones</b></td>";
               echo "</tr>";

               echo "<form action='miscmgr.php' method='POST'>";
               echo "<input type='hidden' name='estado' value='2'>";

               $res = $lnk->query($sql0);
               while ($rows = $res->fetch_array())
               {
                   echo "<input type='hidden' name='id' value='";
                   echo $rows["id"],"'>";

                   echo "<tr id=\"line_list\">";
                   echo "<td>", $rows["nombre"], "</td>";
                   echo "<td>", $rows["obs"], "</td>";
                   echo "<td><input type='submit' value='borrar'></td>";
                   echo "</tr>";
               }

               echo "</form></table><br><br>";

               ?>

                <h3>Agregar nueva categor&iacute;a</h3>
                <form action="miscmgr.php" method="POST">
                <input type="hidden" name="estado" value="1">

                <table width="24%">
                    <tr>
                        <td>
                            <label for="nombre">Categor&iacute;a</label>
                        </td>
                        <td>
                            <input type="text" name="nombre" id="nombre">
                        </td>
                    </tr>
                        <td>
                            <label for="obs">Observaciones</label>
                        </td>
                        <td>
                            <input type="text" name="obs" id="obs">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="reset" value="Reestablecer">
                        </td>
                        <td>
                            <input type="submit" value="Agregar categor&iacute;a">
                        </td>
                    </tr>
                </table>
                </form>

               <?php
        }

        $lnk->close();
    } // Check status (en valor distinto a 1)


    if ($status_ == 1)
    {
        $nombre = $UTILS->clean($_POST["nombre"]);
        $obs = $UTILS->clean($_POST["obs"]);

        $sql1 = "insert into categorias values('','$nombre','$obs')";
        $lnk = $FUNCIONES->conectar("club");
        $res = $lnk->query($sql1);
        $lnk->close();
        echo "<br>Nueva categor&iacute;a de socio agregada: <b>$nombre</b>";
        echo "<br><a href='miscmgr.php'>[ Actualizar pantalla ]</a><br><br>";
    }

    if ($status_ == 2)
    {
        $idc = $UTILS->clean($_POST["id"]);

        $sql1 = "delete from categorias where id=$idc";
        $lnk = $FUNCIONES->conectar("club");
        $res = $lnk->query($sql1);
        $lnk->close();

        echo "<br><a href='miscmgr.php'>[ Actualizar pantalla ]</a><br><br>";
    }
?>

</body>
</html>
