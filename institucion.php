<?php
/*
 * institucion.php
 *
 * Copyright 2015 Abel Send�n <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

?>

<?php require("motor.php") ?>
<?php require("config.php") ?>
<!DOCTYPE html>
<html>
<head>
    <title>GNUClub/institucional</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="generator" content="gvim 7.3" />
    <link rel="Stylesheet" href="estilo.css">
    <link rel="Stylesheet" href="estiloSocio.css">
</head>
<body>

<?php
    $WEB->mainMenu();
    $id = $_GET["fbd39ff8da6d47fe19e560b268815112a6d47fe19ezrf"];
?>

<table width="90%" align="left" id="tab_body">
<tr><td valign="top" align="center">

<h1>Instituci&oacute;n</h1>

<form action="institucion.php" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="enviado" id="enviado" value="1">
		<table width="80%" border="0">
		<tr>
			<td><b><label for="nombre">Nombre</label></b></td>
			<td><input type="text" name="nombre" id="nombre"
				value="<?php echo $CLUB ?>" style="width:320px"></td>
		</tr>
		<tr>
			<td><b><label for="domicilio">Domicilio</label></b></td>
			<td><input type="text" name="domicilio" id="domicilio"
				value="<?php echo $DOMICILIO ?>" style="width:320px"></td>
		</tr>
		<tr>
			<td><b><label for="telefono">Tel&eacute;fono</label></b></td>
			<td><input type="text" name="telefono" id="telefono"
				value="<?php echo $TELEFONO ?>" style="width:320px"></td>
		</tr>
		<tr>
			<td><b><label for="cuit">CUIT / RUT</label></b></td> 
			<td><input type="text" name="cuit" id="cuit"
				value="<?php echo $CUIT ?>" style="width:320px"></td>
		</tr>
		<tr>
			<td><b><label for="logo">Logo</label></b></td>
			<td><img src="fotos/logo.jpg" width="320" height="240">
			<br><input type="file" name="logo" id="logo">
			</td>
		</tr>
		</table>
		<br>
		<br>

		<br><font color="red">Contrase&ntilde;a de administrador de GNU Club</font>
		<br><input type="password" name="pwd1" id="pwd1" style="width:200px">
		<br>
   		<br><input type="submit" value="Modificar informaci&oacute;n" style="width:200px">
</form>

<?php

$modificar = $_POST["enviado"];

if ($modificar == 1) {
	$pwd1 = $_POST["pwd1"];
	$nomb = $UTILS->clean($_POST["nombre"]);
	$domi = $UTILS->clean($_POST["domicilio"]);
	$cuit = $UTILS->clean($_POST["cuit"]);
	$tel = $UTILS->clean($_POST["telefono"]);

	if ($pwd1 == $ADMIN_PASSWORD) {
		$fd = fopen("config.php", "w");
		fwrite($fd, "<?php", 128);
        fwrite($fd, "\n//  Archivo autogenerado por GNU Club, los cambios hechos", 128);
        fwrite($fd, "\n//        aqui seran sobreescritos autom�ticamente.\n\n", 128);
		fwrite($fd, "\n//**** Info de la institucion ******************************", 128);
		fwrite($fd, "\n\$CLUB=\"$nomb\";", 128);
		fwrite($fd, "\n\$CUIT=\"$cuit\";", 128);
		fwrite($fd, "\n\$DOMICILIO=\"$domi\";", 128);
		fwrite($fd, "\n\$TELEFONO=\"$tel\";", 128);
		fwrite($fd, "\n\n", 128);
		fwrite($fd, "\n//**** Contrase�a de root de mysql *************************", 128);
		fwrite($fd, "\n\$DATABASE_PASSWORD=\"$DATABASE_PASSWORD\";", 128);
		fwrite($fd, "\n\n", 128);
		fwrite($fd, "\n//**** Contrase�a de administrador de gnu/club *************", 128);
		fwrite($fd, "\n\$ADMIN_PASSWORD=\"$ADMIN_PASSWORD\";", 128);
		fclose($fd);

		$nLogo = $_FILES["logo"];
		if($nLogo != null) {
			if (is_uploaded_file($_FILES["logo"]["tmp_name"])) {
				 copy($_FILES["logo"]["tmp_name"], "./fotos/logo.jpg");
			}
		}

		echo "<br>Cambios aplicados, vuelva a entrar para que surtan efecto.";
	}

	else {
		echo "<h2 style=\"color:red\">Contrase&ntilde;a incorrecta</h2>";
	}

}

?>

</td></tr>
</table>

</body>
</html>
