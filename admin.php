<?php
/*
 * admin.php
 *
 * Copyright 2015 abel <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
?>

<!DOCTYPE html>

<?php
// Las siguientes lineas son solo para depurar.
// ini_set('display_errors', TRUE);
// ini_set('display_startup_errors', TRUE);

date_default_timezone_set('UTC');
require_once dirname(__FILE__) . '/PHPOffice/PHPExcel/PHPExcel.php';
$objPHPExcel = new PHPExcel();

require("motor.php");
require("config.php");
?>

<head>
	<title>Administrador de base de datos de GNU/Club</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8">
	<meta name="generator" content="Geany 1.24">
    <link rel="stylesheet" href="estilo.css">
</head>

<body>
    <h1>Administrador de base de datos de GNU/Club </h1>
    <?php echo file_get_contents("mnu_admin.html")  ?>

    <h2>Crear una base de datos nueva</h2>
    <form action="admin.php" method="POST">
        <input type="hidden" name="op" value="1">
        <table id="tabla_form">
            <tr>
                <td><label for="pwd1">Contrase&ntilde;a del administrador de GNU/Club</label></td>
                <td><input type="password" id="pwd1" name="pwd1"></td>
            </tr>
            <tr>
                <td> </td>
                <td><input type="submit" value="Crear base de datos"></td>
            </tr>
        </table>
    </form>
    <br>

    <h2>Eliminar la base de datos</h2>
    <form action="admin.php" method="POST">
        <input type="hidden" name="op" value="2">
        <table id="tabla_form">
            <tr>
                <td><label for="pwd1">Contrase&ntilde;a de administrador de GNU/Club</label></td>
                <td><input type="password" id="pwd1" name="pwd1"></td>
            </tr>
            <tr>
                <td> </td>
                <td><input type="submit" value="Borrar base de datos"></td>
            </tr>
        </table>
    </form>
    <br>

    <h2>Restauraci&oacute;n de los datos</h2>
        <table id="tabla_form">
        <form action="admin.php" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="op" value="4">
            <tr>
                <td><label for="planilla">Seleccionar planilla</label></td>
                <td><input type="file" name="planilla" id="planilla"></td>
            </tr>
			<tr>
				<td> <label for="tabla">Tabla a restaurar.</label> </td>
				<td>
					<select name="tabla" id="tabla">
						<option value="socio">Socios</option>
						<option value="cuota">Cuota</option>
						<option value="historial">Historial</option>
                        <option value="categorias">Categor&iacute;as</option>
					</select>
				</td>
			</tr>
			<tr>
				<td><label for="cantRows">Cantidad de filas a importar</label></td>
				<td> <input type="text" name="cantRows" id="cantRows" placeholder="10"></td>
			</tr>
            <tr>
                <td><label for="pwd1">Contrase&ntilde;a de administrador de GNU/Club</label></td>
                <td><input type="password" id="pwd1" name="pwd1"></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input type="submit" value="Importar planilla" name="imp" id="imp">
                </td>
            </tr>
        </form>
        </table>
        <br>

        <h2>Respaldo de los datos</h2>
        <table id="tabla_form">
            <tr>
                <td>
                    <form action="admin.php" method="POST">
                    <input type="hidden" name="op" value="3">
                    <input type="submit" value="Exportar planillas" name="exp" id="exp">
                    </form>
                </td>
            </tr>
        </table>
</body>
<br>

<?php
$op = $_POST["op"];
$pwd1 = $_POST["pwd1"];
$file1 = "tmp/socios.xlsx";
$file2 = "tmp/cuota.xlsx";
$file3 = "tmp/historial.xlsx";
$file4 = "tmp/categorias.xlsx";


    if($op == 1) {
        if($pwd1 == $ADMIN_PASSWORD) {
            $sql0 = "create database club";

            $sql1 = "
            CREATE TABLE  socio (
            id INT UNSIGNED NOT NULL AUTO_INCREMENT,
            nombre varchar(128),
            apellido varchar(128),
            nacimiento date,
            dni	varchar(16),
            sexo varchar(4),
            nacionalidad varchar(128),
            alta date,
            baja date,
            estado_civil varchar(16),
            calle varchar(128),
            numero int(11),
            depto varchar(255),
            cp varchar(16),
            localidad varchar(255),
            provincia varchar(255),
            telefono1 varchar(32),
            telefono2 varchar(32),
            email varchar(255),
            estado varchar(32),
            imagen varchar(128),
			categoria int(3),
            obs varchar(255),
            INDEX(id) )";

            $sql2 = "
            CREATE TABLE IF NOT EXISTS cuota(
            id INT UNSIGNED NOT NULL AUTO_INCREMENT,
            ids INT UNSIGNED,
            fecha DATE,
            tipo TINYINT,
            valor DOUBLE,
            al_dia VARCHAR(3),
            atraso TINYINT,
            INDEX(id) )";

            $sql3 = "
            CREATE TABLE IF NOT EXISTS historial(
            id INT UNSIGNED NOT NULL AUTO_INCREMENT,
            ids INT UNSIGNED,
            fecha DATE,
            tipo VARCHAR(64),
            INDEX(id) )";

            $sql4 = "
            CREATE TABLE IF NOT EXISTS categorias(id INT NOT NULL AUTO_INCREMENT,
            nombre VARCHAR(64),
            obs VARCHAR(256),
            INDEX(id) )";

            $l = $FUNCIONES->conectar("mysql");
            $res0 = $l->query($sql0);
            $l->close();
            $l = $FUNCIONES->conectar("club");

            $res1 = $l->query($sql1);
            $res2 = $l->query($sql2);
            $res3 = $l->query($sql3);
            $res4 = $l->query($sql4);

            echo "<h3>Base de datos creada</h3>";
            echo "<br>Creacion de DB: ", $res0;
            echo "<br>Creacion de tabla socio: ", $res1;
            echo "<br>Creacion de tabla cuota: ", $res2;
            echo "<br>Creacion de tabla historial: ", $res3;
            echo "<br>Creacion de tabla categor&iacute;as: ", $res4;

            $l->close();
        }

        else { echo "Contrase&ntilde;a de administrador incorrecta";  }
    }

    if($op == 2) {
        $pwd1 = $_POST["pwd1"];

        if($pwd1 == $ADMIN_PASSWORD) {
            $sql0 = "drop database club";
            $l = $FUNCIONES->conectar("mysql");
            $res0 = $l->query($sql0);

            echo "<h3>Base de datos eliminada</h3>";
            echo "<br>Estado de la operacion: ", $res0;

            $l->close();
        }
        else { echo "Contrase&ntilde;a de administrador incorrecta";  }
    }

    if($op == 3) {
        $sql0 = "select * from socio";
        $sql1 = "select * from cuota";
        $sql2 = "select * from historial";
        $sql3 = "select * from categorias";

        $l = $FUNCIONES->conectar("club");
        $res0 = $l->query($sql0);
        $res1 = $l->query($sql1);
        $res2 = $l->query($sql2);
        $res3 = $l->query($sql3);


        // Se crea el encabezado de cada una de las planillas.
        $objPHPExcel->getProperties()->setCreator("GNU Club")
             ->setLastModifiedBy("GNU Club")
             ->setTitle("Planilla de backup")
             ->setSubject("Listado principal")
             ->setDescription("Copia de seguridad para ser importada en otra oportunidad.")
             ->setKeywords("GNUClub Excel backup")
             ->setCategory("backups");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		//  Aquí exportamos todas las tablas y las dejamos listas para descargar en formato
		// de Excel 2007.


        //**** Tabla socios ***************************************************
        $i = 0;
        while($rows = $res0->fetch_array()) {
            $i++;
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A$i", $rows["nombre"])
            ->setCellValue("B$i", $rows["apellido"])
            ->setCellValue("C$i", $rows["nacimiento"])
            ->setCellValue("D$i", $rows["dni"])
            ->setCellValue("E$i", $rows["sexo"])
            ->setCellValue("F$i", $rows["nacionalidad"])
            ->setCellValue("G$i", $rows["alta"])
            ->setCellValue("H$i", $rows["baja"])
            ->setCellValue("I$i", $rows["estado"])
            ->setCellValue("J$i", $rows["calle"])
            ->setCellValue("K$i", $rows["numero"])
			->setCellValue("L$i", $rows["depto"])
            ->setCellValue("M$i", $rows["cp"])
            ->setCellValue("N$i", $rows["localidad"])
            ->setCellValue("O$i", $rows["provincia"])
            ->setCellValue("P$i", $rows["telefono1"])
            ->setCellValue("Q$i", $rows["telefono2"])
            ->setCellValue("R$i", $rows["email"])
            ->setCellValue("S$i", $rows["estado"])
            ->setCellValue("T$i", $rows["imagen"])
			->setCellValue("U$i", $rows["categoria"])
            ->setCellValue("V$i", $rows["obs"]);
        }
        echo "$i registros exportados para socios<br>";
        $objPHPExcel->getActiveSheet()->setTitle('Socios');
        $objWriter->save("$file1");


        //**** Tabla cuota ****************************************************
        $i = 0;
        while($rows = $res1->fetch_array()) {
            $i++;
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A$i", $rows["ids"])
            ->setCellValue("B$i", $rows["fecha"])
            ->setCellValue("C$i", $rows["tipo"])
            ->setCellValue("D$i", $rows["valor"])
            ->setCellValue("E$i", $rows["al_dia"])
            ->setCellValue("F$i", $rows["atraso"]);
        }
        echo "$i registros exportados para cuota<br>";
        $objPHPExcel->getActiveSheet()->setTitle('Cuota');
        $objWriter->save("$file2");


        //**** Tabla historial ************************************************
        $i = 0;
        while($rows = $res2->fetch_array()) {
            $i++;
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A$i", $rows["ids"])
            ->setCellValue("B$i", $rows["fecha"])
            ->setCellValue("C$i", $rows["tipo"]);
        }
        echo "$i registros exportados para historial<br>";
        $objPHPExcel->getActiveSheet()->setTitle('Historial');
        $objWriter->save("$file3");


        //**** Tabla categorias ***********************************************
        $i = 0;
        while($rows = $res3->fetch_array()) {
            $i++;
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A$i", $rows["nombre"])
            ->setCellValue("B$i", $rows["obs"]);
        }
        echo "$i registros exportados para categor&iacute;as<br>";
        $objPHPExcel->getActiveSheet()->setTitle('Categorías');
        $objWriter->save("$file4");


        $l->close();
        echo "<br><b>Descargar planillas de respaldo</b>";
        echo "<br><a href='$file1'>Socios</a>";
        echo "<br><a href='$file2'>Cuota</a>";
        echo "<br><a href='$file3'>Historial</a>";
		echo "<br><a href='$file4'>Categor&iacute;as</a>";
    }

    if($op == 4) {
        $pwd1 = $_POST["pwd1"];

        if($pwd1 == $ADMIN_PASSWORD) {
			$cntRows = $_POST["cantRows"];
			$tabla = $_POST["tabla"];

			// Por si se elijió un numero muy bajo (o no se marcó ningun valor) en la cantidad
			// de registros a importar.
			if ($cntRows < 1) {
				$cntRows = 1;
			}

            require_once dirname(__FILE__) . '/PHPOffice/PHPExcel/PHPExcel/IOFactory.php';
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');

			// Aquí subimos la planilla elejida y la copiamos a un archivo temporal.
            $sheet = $_FILES["planilla"];
            if (is_uploaded_file($_FILES["planilla"]["tmp_name"])) {
                copy($_FILES["planilla"]["tmp_name"], "$file1");
            }

			$objPHPExcel = $objReader->load("$file1");
            $x = 0;
			$l = $FUNCIONES->conectar("club");

			// A importar las planillas creadas...
			if ($tabla == "socio") {
				for ($i=1; $i <= $cntRows; $i++) {
					$nombre = $objPHPExcel->getActiveSheet()->getCell("A$i");
	                $apellido = $objPHPExcel->getActiveSheet()->getCell("B$i");
	                $nacimiento = $objPHPExcel->getActiveSheet()->getCell("C$i");
	                $dni = $objPHPExcel->getActiveSheet()->getCell("D$i");
	                $sexo = $objPHPExcel->getActiveSheet()->getCell("E$i");
	                $nacionalidad = $objPHPExcel->getActiveSheet()->getCell("F$i");
	                $alta = $objPHPExcel->getActiveSheet()->getCell("G$i");
	                $baja = $objPHPExcel->getActiveSheet()->getCell("H$i");
	                $estado_civil = $objPHPExcel->getActiveSheet()->getCell("I$i");
	                $calle = $objPHPExcel->getActiveSheet()->getCell("J$i");
	                $numero = $objPHPExcel->getActiveSheet()->getCell("K$i");
	                $depto = $objPHPExcel->getActiveSheet()->getCell("L$i");
	                $cp = $objPHPExcel->getActiveSheet()->getCell("M$i");
	                $localidad = $objPHPExcel->getActiveSheet()->getCell("N$i");
	                $provincia = $objPHPExcel->getActiveSheet()->getCell("O$i");
	                $telefono1 = $objPHPExcel->getActiveSheet()->getCell("P$i");
	                $telefono2 = $objPHPExcel->getActiveSheet()->getCell("Q$i");
	                $email = $objPHPExcel->getActiveSheet()->getCell("R$i");
	                $estado = $objPHPExcel->getActiveSheet()->getCell("S$i");
	                $imagen = $objPHPExcel->getActiveSheet()->getCell("T$i");
					$cat = $objPHPExcel->getActiveSheet()->getCell("U$i");
	                $obs = $objPHPExcel->getActiveSheet()->getCell("V$i");

					$sql0="insert into $tabla values('','$nombre','$apellido',
                     '$nacimiento','$dni','$sexo','$nacionalidad','$alta','$baja',
                     '$estado_civil','$calle','$numero','$depto','$cp','$localidad',
                     '$provincia','$telefono1','$telefono2','$email','$estado',
                     '$imagen','$cat','$obs')";

					$res0 = $l->query($sql0);
					// echo "<br>".$sql0." -  ESTADO $res0";
					$x++;
				}
			}

			if ($tabla == "cuota") {
				for ($i=1; $i <= $cntRows; $i++) {
					$ids = $objPHPExcel->getActiveSheet()->getCell("A$i");
					$fecha = $objPHPExcel->getActiveSheet()->getCell("B$i");
					$tipo = $objPHPExcel->getActiveSheet()->getCell("C$i");
					$valor = $objPHPExcel->getActiveSheet()->getCell("D$i");
					$al_dia = $objPHPExcel->getActiveSheet()->getCell("E$i");
					$atraso = $objPHPExcel->getActiveSheet()->getCell("F$i");

					$sql0="insert into $tabla values('','$ids','$fecha','$tipo','$valor','$al_dia','$atraso')";
					$res0 = $l->query($sql0);
					// echo "<br>".$sql0." -  ESTADO $res0";
					$x++;
				}
			}

			if ($tabla == "historial") {
				for ($i=1; $i <= $cntRows; $i++) {
					$ids = $objPHPExcel->getActiveSheet()->getCell("A$i");
					$fecha = $objPHPExcel->getActiveSheet()->getCell("B$i");
					$tipo = $objPHPExcel->getActiveSheet()->getCell("C$i");

					$sql0="insert into $tabla values('','$ids','$fecha','$tipo')";
					$res0 = $l->query($sql0);
					// echo "<br>".$sql0." -  ESTADO $res0";
					$x++;
				}
			}

            if ($tabla == "categorias") {
				for ($i=1; $i <= $cntRows; $i++) {
					$nombre = $objPHPExcel->getActiveSheet()->getCell("A$i");
					$obs = $objPHPExcel->getActiveSheet()->getCell("B$i");

					$sql0="insert into $tabla values('','$nombre','$obs')";
					$res0 = $l->query($sql0);
					# echo "<br>".$sql0." -  ESTADO $res0";
					$x++;
				}
			}

			echo "<br><br>$x registros importados para $tabla.";
            $l->close();
        }


        else { echo "Contrase&ntilde;a de administrador incorrecta";  }
    }
?>
</html>
