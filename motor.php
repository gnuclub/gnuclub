<?php
/******************************************************************************
 * motor.php
 *
 * Copyright 2015 Abel Sendón <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *****************************************************************************/

/*!
 * \name PHP WebApp Engine, versión
 * \version 4.130108
 * \date 2003-2013
 * \author Abel Sendón
 * \copyright 2012(C)
 * \package Base
 * \brief Espeleto de aplicaciones PHP
 * \details Contiene las clases Funciones y Utilidades.
 */


/*!
 * \class Funciones
 * \brief Funciones básicas
 *  \details Las funciones presentes es ésta clase son las elementales
 *  para la mayoría de las tareas comunes en las páginas creadas
 * (contadores, conectores a base de datos, etc)
 */
class Funciones {
    /**
    * \fn int conectar($base)
    * \brief Conecta y selecciona una base de datos MySQL
    * \param Base El nombre de la base de datos
    * \return Un identificador de recursos de la base de datos
    **/
    function conectar($base)
    {
        require("config.php");
        $lnk = mysqli_connect("localhost", "root", $DATABASE_PASSWORD, $base) or die
        ("Error de conexion MySQLi".mysqli_error($lnk));
        return $lnk;
    }


    /**
     * \fn contador
     * \param pagina Pagina que cuenta con un contador basico.
     * \return void
     * \brief Desde la version 12 del sitio se implementan los contadores
     * \details Para una escritura menor de código el nombre de la página que
     * se le pasa por parámetro debe ir sin la extensión
    **/
    function contador($a)
    {
        // Lectura
        $F = "counter-$a";
        $fd = fopen($F, "r");
        $x = fgets($fd,96);
        fclose($fd);

        // Aumento del valor
        $x += 1;

        // Escritura
        $fd = fopen($F, "w");
        fwrite($fd, "$x",96);
        fclose($fd);
        return $x;
    } // Fin de funcion contador


    function version()
    {
        echo "<br>Versi&oacute;n de la aplicaci&oacute;n: 0.150326";
        echo "<br>PHP WebApp Engine versi&oacute;n 4.130117";
    }


    /**
     * \fn barra
     * \brief Barras para los gráficos de las estadísticas (sin gd)
     * \param X Ubicación dentro del contenedor (desde la izquierda)
     * \param Y Ubicación dentro del contenedor (desde arriba)
     * \param Ancho Ancho que tendrá
     * \param Color El color que tendrá
     * \param Id (Nuevo) identificador que tendrá, util para CSS
     * \return void
    **/
    function barra($X, $Y, $ancho, $color, $id)
    {
        echo "<table id='$id' border=0 height=32 width='$ancho'
        style=\"position:inherit;top:$Y;left:$X\">";
        echo "<tr><td bgcolor=\"$color\"></td></tr>";
        echo "</table>\n";
    }
}

$FUNCIONES = new Funciones();
// Fin de clase Funciones ////////////////////////////////////////////////////



/*!
* \class Utilidades
* \brief Accesorios para la clase Funciones
**/
class Utilidades {
    /**
    * \fn now
    * \brief Imprime la fecha actual en al formato dd-mm-aa
    * \return Un string con la fecha actual
    **/
    public function now($ocultar = 0)
    {
        $f = date("Y-m-d");
        if ($ocultar != 1) {  echo $f;  }
        return $f;
    }


    /**
     * \fn relleno
     * \brief Espaciador vertical
     * \details Fuerza un espacio vacío donde no funcionan los tags p y br
     * \return void
     **/
    public function relleno()
    {
        // Debería cambiar según el estilo del body
        $color = "white";
        echo "<table style=\"background:$color;width:100%\">";
        echo "<tr style=\"background:$color\"><td>&nbsp;</td></tr>";
        echo "<tr style=\"background:$color\"><td>&nbsp;</td></tr></table>";
    }


    function clean($str)
    {
        return htmlentities(strip_tags($str), ENT_QUOTES);
    }
}

$UTILS = new Utilidades();
// Fin de clase Utilidades ///////////////////////////////////////////////////


//  Esta clase debería ir en un archivo separado, pero hay problemas con las
//  inclusiones...
//
class cSocio {
    /*
     * Orden de aparicion en mysql:
     * =============================
     * id int unsigned auto_increment
     * nombre varchar(128)
     * apellido varchar(128)
     * nacimiento date,
     * dni long
     * sexo varchar(4)
     * nacionalidad varchar(128)
     * alta date
     * baja date
     * estado_civil varchar(16)
     * calle varchar(128)
     * numero int
     * depto varchar(255)
     * cp varchar(16)
     * localidad varchar(255)
     * provincia varchar(255)
     * telefono1 varchar(32),
     * telefono2 varchar(32)
     * email varchar(255)
     * estado varchar(32)
     * imagen varchar(128)
     * categoria int(3)
     * obs varchar(255)
     * */

    public function agregar($kwargs)
    {
        extract($kwargs);
        $FUNCIONES = new Funciones();
        $l = $FUNCIONES->conectar("club");
        $sql0 = "insert into socio values(
            '','$nom', '$ape', '$nc1', '$dni', '$sex', '$nc2', '$alt', '', '$est',
            '$cal', '$num', '$dto', '$cp', '$loc', '$pro', '$te1', '$te2', '$eml',
            '$act', '$img', '$cat', '$obs')";
        $l->query($sql0);
        $l->close();
    }


    public function modificar($kwargs)
    {
        extract($kwargs);
        $sql0 = "update socio set
             nombre='$nom',
             apellido='$ape',
             nacionalidad='$nc2',
             estado_civil='$est',
             calle='$cal',
             numero='$num',
             depto='$dto',
             cp='$cp',
             localidad='$loc',
             provincia='$pro',
             telefono1='$te1',
             telefono2='$te2',
             email='$eml',
             dni='$dni',
             categoria='$cat',
             obs='$obs' where id=$idr";

             $FUNCIONES = new Funciones();
             $l = $FUNCIONES->conectar("club");
             $l->query($sql0);
             $l->close();
    }


    public function borrar($ids)
    {
        $sql0 = "delete from socio where id=$ids";
        $FUNCIONES = new Funciones();
        $l = $FUNCIONES->conectar("club");
        $l->query($sql0);
        $l->close();
    }


    public function dar_baja($i, $fecha, $op)
    {
        $FUNCIONES = new Funciones();
        $l = $FUNCIONES->conectar("club");
        $sql="";

        if ($op == 0)
        {
            $sql = "update socio set estado='si', alta='$fecha' where id=$i";
        }

        else
        {
            $sql = "update socio set estado='no', baja='$fecha' where id=$i";
        }

        $l->query($sql);
        $l->close();
    }


    public function es_activo($id)
    {
        $FUNCIONES = new Funciones();
        $l = $FUNCIONES->conectar("club");
        $sql = "select estado from socio where id=$id";
        $res = $l->query($sql);
        $row = $l->fetch_assoc();
        $l->close();
        return $row["estado"];
    }


    public function add_historial($kwargs)
    {
        extract($kwargs);
        $t = "";

        if ($tipo == 0)  {  $t = "Creacion";               }
        if ($tipo == 1)  {  $t = "Cambio de plan";         }
        if ($tipo == 2)  {  $t = "Alta/Reactivacion";      }
        if ($tipo == 3)  {  $t = "Baja";                   }
        if ($tipo == 4)  {  $t = "Eliminacion";            }
        if ($tipo == 5)  {  $t = "Modificacion de datos";  }
        if ($tipo == 6)  {  $t = "Cambio de categoria";    }

        $FUNCIONES = new Funciones();
        $l = $FUNCIONES->conectar("club");
        $sql = "insert into historial values('', $id, '$fecha', '$t')";
        $l->query($sql);
        $l->close();
    }

    public function ver($idr)
    {
        $FUNCIONES = new Funciones();
        $l = $FUNCIONES->conectar("club");
        $sql = "select * from socio where id=$idr";
        $res = $l->query($sql);
        $row = $res->fetch_assoc();
        $l->close();

        $l = $FUNCIONES->conectar("club");
        $sql2 = "select * from categorias where id=".$row["categoria"];
        $res = $l->query($sql2);
        $row2 = $res->fetch_assoc();
        $l->close();

        $estiloAct = "";

        if ($row["estado"] == "no")
        {
            $estiloAct = "color:#F00";
        }

        $sexo="";

        if ($row["sexo"] == "f")
        {
            $sexo = "Femenino";
        }

        else
        {
            $sexo = "Masculino";
        }

        require("config.php");

        echo "<table id=\"dsp_ver_reg\" border=1>";
        echo "<tr><td>";
        echo "<br><b>ID de socio: </b>", $row["id"];
        echo "<br><b>Nombre: </b> ", $row["nombre"];
        echo "<br><b>Apellido: </b>", $row["apellido"];
        echo "<br><b>Tel&eacute;fono de la casa: </b>",$row["telefono1"];
        echo "<br><b>Tel&eacute;fono del trabajo: </b> ",$row["telefono2"];
        echo "<br><b>Fecha de nacimiento: </b> ", $row["nacimiento"];
        echo "<br><b>email: </b><a href=\"mailto:\"", $row["email"],"?Subject:\"$CLUB\">", $row["email"],"</a>";
        echo "<br><b>Cédula/DNI: </b> ", $row["dni"];
        echo "<br><b>Sexo: </b> ", $sexo;
        echo "<br><b>Nacionalidad: </b> ", $row["nacionalidad"];
        echo "<br><b>Estado civil: </b> ", $row["estado_civil"];
        echo "<br><b>Catagor&iacute;a de socio: </b>", $row2["nombre"];
        echo "<br><span style=\"$estiloAct\"><b>Activo/a: </b> ",$row["estado"], "</span>";
        echo "<br><b>Fecha de registro: </b> ", $row["alta"];
        echo "<br><b>Fecha de baja: </b><span style=\"$estiloAct\">", $row["baja"], "</span>";
        echo "<br><b>Calle: </b> ", $row["calle"];
        echo "<br><b>N&uacute;mero: </b> ", $row["numero"];
        echo "<br><b>CP: </b> ", $row["cp"];
        echo "<br><b>Depto: </b> ", $row["depto"];
        echo "<br><b>Localidad: </b> ", $row["localidad"];
        echo "<br><b>Provincia </b> ", $row["provincia"];
        echo "</td><td>";
        echo "<br><img src=\"", $row["imagen"], "\" width=\"360\" height=\"360\">";
        echo "<br><b> Observaciones: </b> <pre>", $row["obs"], "</pre>";
        echo "</td></tr></table>";
    }

    // La funcion getFld develve el valor de un campo, especificando el Nombre
    // corto de este y el id de usuario, es util para evitar escribir multiples
    // consultas (y crear un codigo reducido)
    //
    public function getFld($campo, $i) {
        $FUNCIONES = new Funciones();
        $l = $FUNCIONES->conectar("club");
        $sql = "select * from socio where id=$i";
        $res = $l->query($sql);
        $row = $res->fetch_assoc();
        $res->free();
        $l->close();

        if($campo == "nom")
        {
            echo $row["nombre"]." ";
            return;
        }

        if($campo == "ape")
        {
            echo $row["apellido"]." ";
            return;
        }

        if($campo == "dni")
        {
            echo $row["dni"]." ";
            return;
        }

        if($campo == "te1")
        {
            echo $row["telefono1"]." ";
            return;
        }

        if($campo == "te2")
        {
            echo $row["telefono2"]." ";
            return;
        }

        if($campo == "cp")
        {
            echo $row["cp"]." ";
            return;
        }

        if($campo == "eml")
        {
            echo $row["email"]." ";
            return;
        }

        if($campo == "nc2")
        {
            echo $row["nacionalidad"]." ";
            return;
        }

        if($campo == "est")
        {
            echo $row["estado_civil"]." ";
            return;
        }

        if($campo == "act")
        {
            echo $row["estado"]." ";
            return;
        }

        if($campo == "cal")
        {
            echo $row["calle"]." ";
            return;
        }

        if($campo == "num")
        {
            echo $row["numero"]." ";
            return;
        }

        if($campo == "dto")
        {
            echo $row["depto"]." ";
            return;
        }

        if($campo == "loc")
        {
            echo $row["localidad"]." ";
            return;
        }

        if($campo == "pro")
        {
            echo $row["provincia"]." ";
            return;
        }

        if($campo == "obs")
        {
            echo $row["obs"]." ";
            return;
        }

        if($campo == "cat")
        {
            $l = $FUNCIONES->conectar("club");
            $res = $l->query("select * from categorias where id=".$row["categoria"]);
            $row = $res->fetch_assoc();
            return $row["nombre"];
        }

        $l = $FUNCIONES->conectar("club");
        $res = $l->query("select * from cuota order by id desc limit 1");
        $row = $res->fetch_assoc();
        $res->free();
        $l->close();

        if($campo == "tipo")
        {
            switch ($row["tipo"])
            {
                case '1':
                        echo "mensual";
                        return;
                    break;

                case '2':
                        echo "bimestral";
                        return;
                    break;

                case '3':
                        echo "trimestral";
                        return;
                    break;

                case '4':
                        echo "cuatrimestral";
                        return;
                    break;

                case '6':
                        echo "semestral";
                        return;
                    break;

                case '12':
                        echo "anual";
                        return;
                    break;

                default:
                    return (-2);
                    break;
            }
        }

        if($campo == "valr")
        {
            echo $row["valor"];
            return;
        }

        if($campo == "adia")
        {
            echo $row["al_dia"];
            return;
        }

        if($campo == "lags")
        {
            echo $row["atraso"];
            return;
        }
    }
}

$Socio = new cSocio();
// Fin de clase cSocio ///////////////////////////////////////////////////////



class web {
    public function mainMenu()
    {
        echo "<table id=\"tab_menu\" height=\"1080px\" align=\"left\">";
        echo "<tr><td>";
        echo file_get_contents("mnu.html");
        echo "</td></tr>";

        for ($i = 0; $i < 45; $i++)
        {
            echo "<tr><td align=\"center\" valign=\"top\">
            &nbsp; &nbsp;
            </td></tr>";
        }

        echo "<tr><td align=\"center\">GNUClub</td></tr>";
        echo "</table>";
    }


    public function nbsp($j)
    {
        for ($i = 0; $i < $j; $i++)
        {
            echo "&nbsp;";
        }
        return(0);
    }

}

$WEB = new Web();
// Fin de clase Web //////////////////////////////////////////////////////////

?>
