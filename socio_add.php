<?php
/*
 * socio_add.php
 *
 * Copyright 2015 Abel Sendón <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

require("motor.php");
?>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <title>GNUClub/Socio/Nuevo</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="generator" content="gvim 7.3" />
    <link rel="stylesheet" href="estilo.css">
</head>

<body>

<?php $WEB->mainMenu() ?>
<table width="90%" align="left" id="tab_body">
<tr><td valign="top" align="center">

    <h1>Socio/Nuevo</h1>
    <a href="socio.php"><input type="button" value="Volver al menú socio"></a>

    <br>
    <br>
    <form action="socio__add.php" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="confirmado" value="1">
    <table id="tabla_form">
    <tr>
        <td><label for="nombre">Nombre</label></td>
        <td><input type="text" name="nombre" id="nombre"></td>
    </tr>
    <tr>
        <td><label for="apellido">Apellido</label></td>
        <td><input type="text" name="apellido" id="apellido"></td>
    </tr>
    <tr>
        <td><label for="nacimiento">Fecha de nacimiento (AAAA-MM-DD) </label></td>
        <td><input type="text" name="nacimiento" id="nacimiento"></td>
    </tr>
    <tr>
        <td><label for="nacionalidad">Nacionalidad</label></td>
        <td><input type="text" name="nacionalidad" id="nacionalidad"></td>
    </tr>
    <tr>
        <td><label for="dni">Cédula/DNI</label></td>
        <td><input type="text" name="dni" id="dni"></td>
    </tr>
    <tr>
        <td><label for="sexo">Sexo</label></td>
        <td align="left"><select name="sexo" id="sexo">
            <option value="m">Masculino</option>
            <option value="f">Femenino</option>
        </select></td>
    </tr>
    <tr>
        <td><label for="estado_civil">Estado civil</label></td>
        <td><select name="estado_civil" id="estado_civil">
            <option option"soltero/a">Soltero/a</option>
            <option value"casado/a">Casado/a</option>
            <option value"divorciado/a">Divorciado/a</option>
            <option value"viudo/a">Viudo/a</option >
            <option value"concubinato">Concubinato</option >
        </td>
    </tr>
    <tr>
        <td><label for="telefono1">Teléfono de casa</label></td>
        <td><input type="text" name="telefono1" id="telefono1"></td>
    </tr>
    <tr>
        <td><label for="telefono2">Teléfono de trabajo</label></td>
        <td><input type="text" name="telefono2" id="telefono2"></td>
    </tr>
    <tr>
        <td><label for="email">eMail</label></td>
        <td><input type="text" name="email" id="email"></td>
    </tr>

    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>

    <tr>
        <td><label for="dom_calle">Calle</label></td>
        <td><input type="text" name="dom_calle" id="dom_calle"></td>
    </tr>
    <tr>
        <td><label for="dom_numero">Número</label></td>
        <td><input type="text" name="dom_numero" id="dom_numero"></td>
    </tr>
    <tr>
        <td><label for="dom_cp">Código postal</label></td>
        <td><input type="text" name="dom_cp" id="dom_cp"></td>
    </tr>
    <tr>
        <td><label for="depto">Departamento</label></td>
        <td><input type="text" name="depto" id="depto"></td>
    </tr>
    <tr>
        <td><label for="dom_localidad">Localidad</label></td>
        <td><input type="text" name="dom_localidad" id="dom_localidad"></td>
    </tr>
    <tr>
        <td><label for="dom_provincia">Provincia</label></td>
        <td><input type="text" name="dom_provincia" id="dom_provincia"></td>
    </tr>

    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>

    <tr>
        <td><label for="activo">Inicia como activo</label></td>
        <td><select name="activo" id="activo">
            <option value="si">Si</option>
            <option value="no">No</option>
        </select>
        </td>
    </tr>
    <tr>
        <td><label for="tipo_plan">Tipo de plan</label></td>
        <td>
            <select name="tipo_plan">
                <option value="1">Mensual</option>
                <option value="2">Bimestral</option>
                <option value="3">Trimestral</option>
                <option value="4">Cuatrimestral</option>
                <option value="6">Semestral</option>
                <option value="12">Anual</option>
            </select>
        </td>
    </tr>
    <tr>
        <td><label for="cat">Categor&iacute;a</label></td>
        <td>
            <select name="cat" id="cat">
                <?php
                    $sql = "select * from categorias";
                    $l = $FUNCIONES->conectar("club");
                    $res = $l->query($sql);
                    while ($row = $res->fetch_array()) {
                        echo "<option value='".$row["id"]."'>";
                        echo $row["nombre"];
                        echo "</option>";
                    }
                    $l->close();
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td><label for="fecha_registro">Fecha de registro</label></td>
    <td><input type="text" name="fecha_registro" id="fecha_registro" value="<?php $UTILS->now()?>"></td>
    </tr>
        <tr>
        <td><label for="obs">Observaciones</label></td>
        <td><input type="text" name="obs" id="obs"></td>
    </tr>
    </tr>
        <tr>
        <td><label for="foto">Seleccionar foto</label></td>
        <td><input type="file" name="foto" id="foto"></td>
    </tr>

    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>

    <tr>
        <td align="right"><input type="submit" value="Registrar"></td>
        <td><input type="reset" value="Resetear"></td>
    </tr>
    </table>
    </form>

</td></tr>
</table>

</body>
</html>
