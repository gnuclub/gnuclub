<?php
/*
 * plan_ver.php
 *
 * Copyright 2015 Abel Sendón <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

?>

<?php
require("motor.php");
require("config.php");
?>

<!DOCTYPE html>
<html>
<head>
    <title>GNUClub/Plan</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="generator" content="Geany 1.23.1" />
    <link rel="stylesheet" href="estilo.css">
</head>
<body>

<?php
$WEB->mainMenu();
$club_ = $CLUB;
$i = $_GET["fbd39ff8da6d47fe19e560b268815112a6d47fe19ezrf8b734619e603d042780a07c"];
if(!$i) {
    $i = $_POST["ids"];
    if(!$i) {
        $dni = $_POST["dni"];
        $link = $FUNCIONES->conectar("club");
        $sql = "select id from socio where dni=$dni";
        $res = $link->query($sql);
        $row = $res->fetch_assoc();
        $i = $row["id"];
        $link->close();
    }
}
?>

<table width="90%"  align="left" id="tab_body">
    <tr>
    <td align="center" valign="top">

    <h1>Plan de abono</h1>
    <form action="plan_ver.php" method="POST">
    <input type="hidden" name="confirm" id="confirm" value="1">
    <input type="hidden" name="ids" id="ids" value="<?php echo $i ?>">
    <table id="tabla_form">

    <tr>
        <td>ID de socio: </td>
        <td> <?php echo $i;
            echo " (";
            $Socio->getFld("nom", $i);
            echo ", ";
            $Socio->getFld("ape", $i);
            echo ") ";
            ?>
        </td>
    </tr>
    <tr>
        <td>Tipo de plan actual: </td>
        <td> <?php $Socio->getFld("tipo", $i) ?> </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
        <td><label for="tipo">Tipo de abono</label></td>
        <td><select name="tipo" id="tipo">
        <option value="1">Mensual</option>
        <option value="2">Bimestral</option>
        <option value="3">Trimestral</option>
        <option value="4">Cuatrimestral</option>
        <option value="6">Semestral</option>
        <option value="12">Anual</option>
        </td>
    </tr>
    <tr>
        <td>Est&aacute; al dia? </td>
        <td><select name="aldia" id="aldia">
             <option value="si">Si</option>
             <option value="no">No</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>Cuantas cuotas de atraso tiene?</td>
        <td><input type="text" name="atraso" id="atraso" placeholder="0" value="0"> </td>
    </tr>
    <tr>
        <td><label for="monto">Monto a abonar</label></td>
        <td><input type="text" name="monto" id="monto" value="0"></td>
    </tr>
    <tr>
        <td>(0 si s&oacute;lo va a cambiar el tipo de plan)</td>
        <td><input type="submit" value="Asignar / Cobrar"></td>
    </tr>


<?php
$confirmar = $UTILS->clean($_POST["confirm"]);

if($confirmar == 1)  {
    $id = $UTILS->clean($_POST["ids"]);
    $fecha = $UTILS->now(1);
    $tipo = $UTILS->clean($_POST["tipo"]);
    $valor = $UTILS->clean($_POST["monto"]);
    $al_dia = $UTILS->clean($_POST["aldia"]);;
    $atraso = $UTILS->clean($_POST["atraso"]);;

    $l = $FUNCIONES->conectar("club");
    $sql1="insert into cuota values('', $id, '$fecha', $tipo, $valor, '$al_dia', $atraso)";
    $l->query($sql1);
    $l->close();

    if($valor == 0) {
        $data2 = array("id" => $id, "fecha" => $UTILS->now(1), "tipo" => 1);
        $Socio->add_historial($data2);
        echo "<h2>Nuevo plan asignado: ";
        if ($tipo == 1)  { echo "mensual"; }
        if ($tipo == 2)  { echo "bimestral"; }
        if ($tipo == 3)  { echo "trimestral"; }
        if ($tipo == 4)  { echo "cuatrimestral"; }
        if ($tipo == 6)  { echo "semestral"; }
        if ($tipo == 12) { echo "anual"; }
        echo "</h2>";
    }
    else {
        echo "<h2>cuota abonada</h2>";
    }
}

?>

    </table>
    </form>

    </td>
    </tr>

</table>
</body>
</html>
