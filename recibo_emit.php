<?php
/*
 * recibo_emit.php
 *
 * Copyright 2015 Abel Sendón <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
?>



<!DOCTYPE html>
<html>
<head>
    <title>GNUClub/Recibo</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="generator" content="Geany 1.24" />
    <link rel="stylesheet" href="estilo.css">
</head>
<body>

<?php 
    require("motor.php");
    require("config.php");

    $id = $_POST["ids"];
    $tipo = $_POST["tipo_valor"];
    $num = $_POST["num"];
    $banco = $_POST["banco"];
    $fecha = $_POST["fecha"];
    $monto = $_POST["monto"];
    $plan = $_POST["plan"];
?>

<table width="90%"  align="left" id="tab_body">
    <tr>
        <td align="center" valign="top">
        <h1>Recibo/emitir</h1>
        <br>

        <hr><br>
        <table border="1" width="60%">
        <!-- encabezado -->
        <tr> 
            <td align="center"><h2><?php echo $CLUB ?></h2></td>
            <td width="50%">
                <h3>Recibo</h3>
                &nbsp;Nº: 
                <br><br>&nbsp;Fecha: <?php $UTILS->now() ?>
                <br>&nbsp;Lugar: <?php echo $DOMICILIO ?>
                <br>&nbsp;CUIT: <?php echo $CUIT ?>
            </td>
        </tr>
        <!-- cuerpo -->
        <tr>
            <td>
                Señor/es: <?php $Socio->getFld("nom", $id) ?> 
                &nbsp; <?php $Socio->getFld("ape", $id) ?>
                <br>C&eacute;dula: <?php $Socio->getFld("dni", $id) ?>
                <br>Plan: <?php $Socio->getFld("tipo", $id) ?>
            </td>
            <td>
                Domicilio:  <?php $Socio->getFld("cal", $id) ?>
                &nbsp; <?php $Socio->getFld("num", $id) ?>, 
                &nbsp; <?php $Socio->getFld("loc", $id) ?>
                <br>Tel. <?php $Socio->getFld("te1", $id) ?>

            </td>
        </tr>
        </table>
        <table border="0" width="60%" id="tabla_form">
        <tr>
            <td>
                <br>Recibimos de <?php $Socio->getFld("nom", $id) ?> <?php $Socio->getFld("ape", $id) ?> 
                la cantidad de $<?php echo $monto ?>. 
            </td>
        </tr>
        </table>
        <!-- pie -->
        <table width="60%" border="1">
        <tr>
        <td width="20%"><b>Tipo de valor</b></td>
        <td width="25%"><b>Cargo banco</b></td>
        <td width="25%"><b>Nº cheque/doc.</b></td>
        <td width="15%"><b>Fecha dep</b></td>
        <td width="15%"><b>Importe</b></td>
        </tr>
        <tr>
        <td width="20%"><?php echo $tipo ?></td>
        <td width="25%"><?php echo $banco ?></td>
        <td width="25%"><?php echo $num ?></td>
        <td width="15%"><?php echo $fecha ?></td>
        <td width="15%">$<?php echo $monto ?></td>
        </tr>
        </table>
        <table width="60%" id="tabla_form">
        <tr><td width="40%">&nbsp;</td><td width="60%">&nbsp;</td></tr>
        <tr>
            <td width="40%">Firma: </td>
            <td width="60%">Aclaración: </td>
        </tr>
        <tr><td width="40%">&nbsp;</td><td width="60%">&nbsp;</td></tr>
        </table>
        <table width="60%" id="tabla_form">
        <tr>
            <td align="center"><h3>Original</h3></td>
        </tr>
        </table>


        <br><hr><br>


        <table border="1" width="60%">
        <!-- encabezado -->
        <tr> 
            <td align="center"><h2><?php echo $CLUB ?></h2></td>
            <td width="50%">
                <h3>Recibo</h3>
                &nbsp;Nº: 
                <br><br>&nbsp;Fecha: <?php $UTILS->now() ?>
                <br>&nbsp;Lugar: <?php echo $DOMICILIO ?>
                <br>&nbsp;CUIT: <?php echo $CUIT ?>
            </td>
        </tr>
        <!-- cuerpo -->
        <tr>
            <td>
                Señor/es: <?php $Socio->getFld("nom", $id) ?> 
                &nbsp; <?php $Socio->getFld("ape", $id) ?>
                <br>C&eacute;dula: <?php $Socio->getFld("dni", $id) ?>
                <br>Plan: <?php $Socio->getFld("tipo", $id) ?>
            </td>
            <td>
                Domicilio:  <?php $Socio->getFld("cal", $id) ?>
                &nbsp; <?php $Socio->getFld("num", $id) ?>, 
                &nbsp; <?php $Socio->getFld("loc", $id) ?>
                <br>Tel. <?php $Socio->getFld("te1", $id) ?>
            </td>
        </tr>
        </table>
        <table border="0" width="60%" id="tabla_form">
        <tr>
            <td>
                <br>Recibimos de <?php $Socio->getFld("nom", $id) ?> <?php $Socio->getFld("ape", $id) ?> 
                la cantidad de $<?php echo $monto ?>. 
            </td>
        </tr>
        </table>
        <!-- pie -->
        <table width="60%" border="1">
        <tr>
        <td width="20%"><b>Tipo de valor</b></td>
        <td width="25%"><b>Cargo banco</b></td>
        <td width="25%"><b>Nº cheque/doc.</b></td>
        <td width="15%"><b>Fecha dep</b></td>
        <td width="15%"><b>Importe</b></td>
        </tr>
        <tr>
        <td width="20%"><?php echo $tipo ?></td>
        <td width="25%"><?php echo $banco ?></td>
        <td width="25%"><?php echo $num ?></td>
        <td width="15%"><?php echo $fecha ?></td>
        <td width="15%">$<?php echo $monto ?></td>
        </tr>
        </table>
        <table width="60%" id="tabla_form">
        <tr><td width="40%">&nbsp;</td><td width="60%">&nbsp;</td></tr>
        <tr>
            <td width="40%">Firma: </td>
            <td width="60%">Aclaración: </td>
        </tr>
        <tr><td width="40%">&nbsp;</td><td width="60%">&nbsp;</td></tr>
        </table>
        <table width="60%" id="tabla_form">
        <tr>
            <td align="center"><h3>Duplicado</h3></td>
        </tr>
        </table>


    </td>
    </tr>
</table> 

</body>
</html>
