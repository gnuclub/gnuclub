<?php
/*
 * socio_del.php
 *
 * Copyright 2015 Abel Sendón <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

?>

<?php require("motor.php") ?>
<!DOCTYPE html>
<html>
<head>
    <title>GNUClub/Socio/Eliminar</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="generator" content="gvim 7.3" />
    <link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" href="estiloSocio.css">
</head>
<body>

<?php $WEB->mainMenu() ?>
<table width="90%" align="left" id="tab_body">
<tr><td valign="top" align="center">

<h1>Socio/Eliminar</h1>
<br>
<?php
$id = $_GET["fbd39ff8da6d47fe19e560b268815112a6d47fe19ezrf"];
$src = $_GET["8141807c2aaf8b734619e603d042780a07c2aaf8b734"];

$l =  $FUNCIONES->conectar("club");
$sql = "select nombre,apellido,dni from socio where id=$id";
$res = $l->real_query($sql);
$l->close();

echo "<h2 style=\"color:#F00\">Está a punto de eliminar de la base de datos al socio ";
echo "con ID $id <br>( ", $row["nombre"], $row["apellido"], " c&eacute;dula Nº", $row["dni"], ")<h2>";
echo "<br><h2 style=\"color:#F40\">Est&aacute; seguro?</h2>";
?>

<br>
<form action="socio_del.php" method="POST">
<input type="hidden" name="confirmar" id="confirmar" value="1">
<input type="hidden" name="ids" id="ids" value="<?php echo $id ?>">
<input type="submit" value="     Si, eliminar     ">
<form><br><br>

<?php
$c = $_POST["confirmar"];

if($c == 1) {
    $id = $_POST["ids"];
    $Socio->borrar($id);

    $data2 = array("id" => $id, "fecha" => $UTILS->now(1), "tipo" => 4);
    $Socio->add_historial($data2);

    echo "<h2 style=\"color:#0F0\">El socio con ID $id ha sido eliminado</h2>";
}

if($src == "lst") {
    echo "<a href=\"socio_lst.php\"><input type='button' value='No, volver a la lista'></a>";
}

else if($src == "ver") {
    echo "<a href=\"socio_ver.php\"><input type='button' value='No, volver a info de socio'></a>";
}

else {
    echo "<a href=\"socio.php\"><input type='button' value='Volver al men&uacute; socio'></a>";
}
?>



