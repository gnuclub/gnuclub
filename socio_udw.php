<?php
/*
 * socio_udw.php
 *
 * Copyright 2015 Abel Sendón <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

?>

<?php require("motor.php") ?>
<!DOCTYPE html>
<html>
<head>
    <title>GNUClub/Socio/Reactivar</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="generator" content="gvim 7.3" />
    <link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" href="estiloSocio.css">
</head>
<body>

<?php
$WEB->mainMenu();  
$ids = $_GET["id"];
?>

<table width="90%" align="left" id="tab_body">
<tr><td valign="top" align="center">

<h1>Socio/Alta</h1>
<br>
<h3>Quitar la baja al socio con ID <?php echo $ids ?></h3>
<form action="socio_udw.php" method="POST"> 
<input type="hidden" id="conf" name="conf" value="1">
<input type="hidden" id="ids" name="ids" value="<?php echo $ids ?>"> 
<input type="hidden" id="fecha" name="fecha" value=<?php $UTILS->now() ?>> 
<a href="socio_ver.php"><input type="button" value="   Volver   "></a>&nbsp;
<input type="submit" value="Reactivar">
</form>


<?php 
$confirm = $_POST["conf"];
if($confirm == 1) { 
    $fecha = $UTILS->clean($_POST["fecha"]);
    $ids = $_POST["ids"];
    $Socio->dar_baja($ids, $fecha, 0);
    
    echo "<br><br><h2>La cuenta de socio ha sido reactivada.</h2>";
    
    $data2 = array("id" => $ids, "fecha" => $fecha, "tipo" => 2);
    $Socio->add_historial($data2);

}

?>

</td></tr>
</table>

</body>
</html>
