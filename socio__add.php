<?php
/*
 * socio_add.php
 *
 * Copyright 2015 Abel Sendón <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

require("motor.php");
?>


<!DOCTYPE html>
<html lang="es">

<head>
    <title>GNUClub/Socio/Nuevo</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="generator" content="gvim 7.3" />
    <link rel="stylesheet" href="estilo.css">
</head>

<body>

<?php $WEB->mainMenu() ?>
<table width="90%" align="left" id="tab_body">
<tr><td valign="top" align="center">

    <h1>Socio/Nuevo</h1>
    <br>
    <a href="socio.php"><input type="button" value="Volver al menú socio"></a>
    <br><br>
    <a href="socio_add.php"><input type="button" value="       Atrás        "></a>

<?php
$confirmar = $_POST["confirmado"];
if($confirmar == 1) {
    /**** Obtenemos el último id y preparamos el siguiente ********************/
    $l = $FUNCIONES->conectar("club");
    $sql = "select id from socio order by id desc limit 0,1";
    $res = $l->query($sql);
    $row = $res->fetch_assoc();
    $tmpID = $row["id"];
    $tmpID += 1;
    $img = "./fotos/foto-id-$tmpID.jpg";
    /**************************************************************************/

    $data = array(
        "nom" => $UTILS->clean($_POST["nombre"]),
        "ape" => $UTILS->clean($_POST["apellido"]),
        "nc1" => $UTILS->clean($_POST["nacimiento"]),
        "dni" => $UTILS->clean($_POST["dni"]),
        "sex" => $UTILS->clean($_POST["sexo"]),
        "nc2" => $UTILS->clean($_POST["nacionalidad"]),
        "alt" => $UTILS->clean($_POST["fecha_registro"]),
        "est" => $UTILS->clean($_POST["estado_civil"]),
        "cal" => $UTILS->clean($_POST["dom_calle"]),
        "num" => $UTILS->clean($_POST["dom_numero"]),
        "loc" => $UTILS->clean($_POST["dom_localidad"]),
        "pro" => $UTILS->clean($_POST["dom_provincia"]),
        "cp"  => $UTILS->clean($_POST["dom_cp"]),
        "te1" => $UTILS->clean($_POST["telefono1"]),
        "te2" => $UTILS->clean($_POST["telefono2"]),
        "eml" => $UTILS->clean($_POST["email"]),
        "act" => $UTILS->clean($_POST["activo"]),
        "dto" => $UTILS->clean($_POST["depto"]),
        "img" => $img,
        "cat" => $UTILS->clean($_POST["cat"]),
        "obs" => $UTILS->clean($_POST["obs"])
    );

    $foto = $_FILES["foto"];
    if (is_uploaded_file($_FILES["foto"]["tmp_name"])) {
        copy($_FILES["foto"]["tmp_name"], "$img");
    }

    $Socio->agregar($data);
    echo "<br><br> Nuevo socio registrado: ";
    $Socio->ver($tmpID);

    $data2 = array("id" => $tmpID, "fecha" => $UTILS->now(1), "tipo" => 0);
    $Socio->add_historial($data2);

	$l = $FUNCIONES->conectar("club");
	$t = $UTILS->clean($_POST["tipo_plan"]);
	$f = $UTILS->now(1);
    $sql="insert into cuota values('', $tmpID, '$f', $t, 0, 'si', 0)";
    $l->query($sql);
    $l->close();

    echo "<br><br><b>Tipo de plan:</b> ";
    if ($Socio->getFld("tipo", $tmpID) == -2) {
        echo "<br><a href=\"plan_ver.php?fbd39ff8da6d47fe19e560b268815112a6d47fe19ezrf8b734619e603d042780a07c=$tmpID\">Modificar tipo de plan</a>";
    }
}
?>

</td></tr>
</table>

</body>
</html>
