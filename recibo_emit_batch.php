<?php
/*
 * recibo_emit_batch.php
 *
 * Copyright 2015 abel <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

?>
<!DOCTYPE html lang="es">

<?php
    require("motor.php");
    require("config.php");
?>

<head>
    <title>GNUClub/Recibo</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="generator" content="Geany 1.24" />
    <link rel="stylesheet" href="estilo.css">
</head>

<body>
    <?php
        $sql0 = "";
        $mesActual = date("m");

        $plan = $_POST["tipo_plan"];
        $monto = $_POST["monto"];
        $tipo = $_POST["tipo"];
        $categoria = $_POST["categoria"];


        if ($tipo == 1)
        {
            $sql0 = "select ids from cuota where tipo='$plan'";
        }

        if ($tipo == 2)
        {
            $sql0 = "select id as ids from socio where categoria='$categoria'";
        }

        switch ($mesActual)
        {
            case '1':  $mesActual = "enero";
                break;
            case '2':  $mesActual = "febrero";
                break;
            case '3':  $mesActual = "marzo";
                break;
            case '4':  $mesActual = "abril";
                break;
            case '5':  $mesActual = "mayo";
                break;
            case '6':  $mesActual = "junio";
                break;
            case '7':  $mesActual = "julio";
                break;
            case '8':  $mesActual = "agosto";
                break;
            case '9':  $mesActual = "septiembre";
                break;
            case '10': $mesActual = "octubre";
                break;
            case '11': $mesActual = "noviembre";
                break;
            default:   $mesActual = "diciembre";
                break;
        }


        $lnk = $FUNCIONES->conectar("club");
        $res0 = $lnk->query($sql0);

        $tipoDePlan = "";
        $numRecibo = 0;

        while ($rows = $res0->fetch_array()) {
            $numRecibo = $FUNCIONES->contador("recibo");

            switch ($plan)
            {
                case '1':  $tipoDePlan = "<br>Recibo mensual - ";
                break;
                case '2':  $tipoDePlan = "<br>Recibo bimestral - ";
                break;
                case '3':  $tipoDePlan = "<br>Recibo trimestral - ";
                break;
                case '4':  $tipoDePlan = "<br>Recibo cuatrimestral - ";
                break;
                case '6':  $tipoDePlan = "<br>Recibo semestral - ";
                break;
                case '12': $tipoDePlan = "<br>Recibo anual - ";
                break;
                default:   $tipoDePlan = "<br> ";
                break;
            }

            # Talon del recibo *************************************************
            echo "<table width=\"75%\" align=\"center\">";
            echo "<tr><td width=\"30%\" id=\"tab_recibo\">\n";
            echo "<table width=\"100%\" height=\"24px\"><tr>";
            echo "<td align=\"left\" valign='top' width='72'>";
            echo "<img src=\"fotos/logo.jpg\" width='53' height='40'></td>";
            echo "<td><h4 style=\"font-size:36px\">".$CLUB."</h4></td>\n";
            echo "<td align=\"right\" valign=\"top\"><h3>\$".$monto."</h3></td>\n";
            echo "</tr></table>";

            echo "<span id=\"fnt_recibo\">";
            echo "Recibo del mes de ".$mesActual;
            echo "<br>Socio N° ".$rows["ids"]." - ";
            echo $Socio->getFld("nom",$rows["ids"])."&nbsp;&nbsp".$Socio->getFld("ape", $rows["ids"]);
            echo "<br>c&eacute;dula/DNI ",$Socio->getFld("dni",$rows["ids"]),", tel: ",$Socio->getFld("te1",$rows["ids"]);

            echo $tipoDePlan," categor&iacute;a ",$Socio->getFld("cat",$rows["ids"]);
            echo "<br>N° ".$numRecibo;
            echo " - duplicado (control) </td><td id=\"tab_recibo\">\n";
            echo "</span>";

            # Cuerpo del recibo ************************************************

            echo "<table width=\"100%\"><tr>\n";
            echo "<td align=\"left\" valign='top' width='72'>";
            echo "<img src=\"fotos/logo.jpg\" width='64' height='48'></td>";
            echo "<td align=\"left\"><h3 style=\"font-size:36px\">".$CLUB."</h3></td>\n";
            echo "<td valign=\"top\">Domicilio: $DOMICILIO - Tel: $TELEFONO";
            echo "<br>CUIT/RUT: $CUIT ";
            echo "<br>Recibo mes de ".$mesActual."</td>";
            echo "<td align=\"right\" valign=\"top\"><h3>\$".$monto."</h3></td>\n";
            echo "</tr></table>\n";

            echo "<span id=\"fnt_recibo\">";
            echo "Socio N° ".$rows["ids"]." - ";
            echo $Socio->getFld("nom", $rows["ids"])."  ".$Socio->getFld("ape", $rows["ids"]);
            echo "c&eacute;dula/DNI ",$Socio->getFld("dni",$rows["ids"]),", tel: ",$Socio->getFld("te1",$rows["ids"]);
            echo $tipoDePlan," categor&iacute;a ",$Socio->getFld("cat",$rows["ids"]);
            echo "<br>N° ".$numRecibo;
            echo " - original (socio)<br><br>\n";

            $WEB->nbsp(2);
            echo "Cobro en: _ _ _ _ _ _ _ _ _ _ _";
            $WEB->nbsp(3);
            echo "Tesorero: _ _ _ _ _ _ _ _ _ _ _";
            $WEB->nbsp(3);
			echo "Presidente: _ _ _ _ _ _ _ _ _ _ _";
            echo "</span><br><br>";
            echo "</td></tr></table>\n";
        }

        $lnk->close();
    ?>

</body>
</html>
