<?php
/*
 * app.php
 *
 * Copyright 2015 Abel Sendón <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

class cSocio {
    private $ID;
    private $nombre;
    private $apellido;
    private $telefono1;
    private $telefono2;
    private $nacimiento;
    private $email;
    private $dni;
    private $sexo;
    private $nacionalidad;
    private $estadoCivil;
    private $activo;
    private $fechaRegistro;
    private $fechaBaja;
    private $domCalle;
    private $domNumero;
    private $domCP;
    private $domDepto;
    private $domLocalidad;
    private $domProvincia;
    private $img;
    private $obs;


    /*
     * Orden de aparicion en mysql:
     * =============================
     * id int unsigned auto_increment
     * nombre varchar(128)
     * apellido varchar(128)
     * nacimiento date,
     * dni long
     * sexo varchar(4)
     * nacionalidad varchar(128)
     * alta date
     * baja date
     * estado_civil varchar(16)
     * calle varchar(128)
     * numero int
     * depto varchar(255)
     * cp varchar(16)
     * localidad varchar(255)
     * provincia varchar(255)
     * telefono1 varchar(32),
     * telefono2 varchar(32)
     * email varchar(255)
     * estado varchar(32)
     * imagen varchar(128)
     * obs varchar(255)
     *
     * */


    public function agregar($id,
         $nom, $ape, $nc1, $dni,
         $sex, $nc2, $alt, $est,
         $cal, $num, $cp,  $loc,
         $pro, $te1, $te2, $eml,
         $act, $dto, $img, $obs)
    {
        #$Fn = new Funciones();
        require("motor.php");
        
        $link = $FUNCIONES->conectar("club");
        $sql0 = "insert into socio values(
        '','$nom', '$ape', '$nc1', '$dni', '$sex', '$nc2', '$alt', '', '$est', '$cal', '$num',
        '$dto', '$cp', '$loc', '$pro', '$te1', '$te2', '$eml', '$act', '$img', '$obs')";
        $result = mysql_query($sql0);
        mysql_close($link);

        $this->ID = $id;
        $this->nombre = $nom;
        $this->apellido = $ape;
        $this->telefono1 = $te1;
        $this->telefono2 = $te2;
        $this->nacimiento = $nc1;
        $this->email = $eml;
        $this->dni = $dni;
        $this->sexo = $sex;
        $this->nacionalidad = $nc2;
        $this->estadoCivil = $est;
        $this->activo = $act;
        $this->fechaRegistro = $alt;
        $this->fechaBaja = "";
        $this->domCalle = $cal;
        $this->domNumero = $num;
        $this->domCP = $cp;
        $this->domDepto = $dto;
        $this->domLocalidad = $loc;
        $this->domProvincia = $pro;
        $this->img = $img;
        $this->obs = $obs;
    }


    public function ver($idr) {
        if($this->ID != $idr) {
            $link = $FUNCIONES->conectar("club");
            $sql = "select id from socio order by id desc limit 0,1";
            $res = mysql_query($sql);
            $row = mysql_fetch_row($res);

            $this->ID = $row["id"];
            $this->nombre = $row["nombre"];
            $this->apellido = $row["apellido"];
            $this->telefono1 = $row["telefono1"];
            $this->telefono2 = $row["telefono2"];
            $this->nacimiento = $row["nacimiento"];
            $this->email = $row["email"];
            $this->dni = $row["dni"];
            $this->sexo = $row["sexo"];
            $this->nacionalidad = $row["nacionalidad"];
            $this->estadoCivil = $row["estado_civil"];
            $this->activo = $row["estado"];
            $this->fechaRegistro = $row["alta"];
            $this->fechaBaja = $row["baja"];
            $this->domCalle = $row["calle"];
            $this->domNumero = $row["numero"];
            $this->domCP = $row["cp"];
            $this->domDepto = $row["depto"];
            $this->domLocalidad = $row["localidad"];
            $this->domProvincia = $row["provincia"];
            $this->img = $row["imagen"];
            $this->obs = $row["obs"];
        
            mysql_close($link);
        }

        echo "<br>Nombre: ", $this->nombre;
        echo "<br>Apellido: ", $this->apellido;
        echo "<br>Tel&eacute;fono de la casa: ", $this->telefono1;
        echo "<br>Tel&eacute;fono del trabajo:", $this->telefono2;
        echo "<br>Fecha de nacimiento: ", $this->nacimiento;
        echo "<br>email: ", $this->email;
        echo "<br>DNI: ", $this->dni;
        echo "<br>Sexo: ", $this->sexo;
        echo "<br>Nacionalidad: ", $this->nacionalidad;
        echo "<br>Estado civil: ", $this->estadoCivil;
        echo "<br>Activo: ", $this->activo;
        echo "<br>Fecha de registro: ", $this->fechaRegistro;
        echo "<br>Fecha de baja: ", $this->fechaBaja;
        echo "<br>Calle: ", $this->domCalle;
        echo "<br>N&uacute;mero: ", $this->domNumero;
        echo "<br>CP: ", $this->domCP;
        echo "<br>Depto: ", $this->domDepto;
        echo "<br>Localidad: ", $this->domLocalidad;
        echo "<br>Provincia ", $this->domProvincia;
        echo "<br><img src=\"", $this->img, "\">";
        echo "<br><pre>", $this->obs, "</pre>";
    }


    public function modificar() {

    }


    public function borrar() {

    }


    public function pagar_cuota() {

    }
}

$Socio = new cSocio();
?>
