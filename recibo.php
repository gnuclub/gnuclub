<?php
/*
 * recibo.php
 *
 * Copyright 2015 Abel Sendón <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

?>

<?php
    require("motor.php");
    require("config.php");
    $id = $_GET["fbd39ff8da6d47fe19e560b268815112a6d47fe19ezrf"];
?>

<!DOCTYPE html>
<html>
<head>
    <title>GNUClub/Recibo</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="generator" content="Geany 1.23.1" />
    <link rel="stylesheet" href="estilo.css">
</head>
<body>

<?php
    $WEB->mainMenu();
?>

<table width="90%"  align="left" id="tab_body">
    <tr>
        <td align="center" valign="top">
        <h1>Recibo</h1>
        <br>
        <h2>Emisi&oacute;n individual</h2>
        <form action="recibo_emit.php" method="POST">
        <table id="tabla_form" width="28%">
            <?php
            if(!$id)
            {
                ?>
                <tr>
                    <td><label for="ids">ID de socio</label></td>
                    <td><input type="number" name="ids" id="ids"></td>
                </tr>
                <?php
            };
            if($id)
            {
                echo "<input type=\"hidden\" name=\"ids\" id=\"ids\" value=\"$id\">";
                echo "ID de usuario: <b>$id</b><br><br>";
            }
            ?>
           <tr>
                <td><label for="tipo_valor">Tipo de valor</label></td>
                <td>
                    <select name="tipo_valor">
                    <option value="efectivo">Efectivo</option>
                    <option value="cheque">Cheque</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><label for="banco">Banco</label></td>
                <td><input type="text" name="banco" id="banco"></td>
            </tr>
            <tr>
                <td><label for="num">Nº cheque documento</label></td>
                <td><input type="text" name="num" id="num"></td>
            </tr>
            <tr>
                <td><label for="fecha">Fecha de depósito</label></td>
                <td><input type="text" name="fecha" id="fecha"></td>
            </tr>
            <tr>
                <td><label for="monto">Monto</label></td>
                <td><input type="text" name="monto" id="monto"></td>
            </tr>
            <tr>
                <td align="right"><input type="submit" value="Generar "></td>
                <td><input type="reset" value="Resetear"></td>
            </tr>
        </table>
        </form>

        <br><br><br>
        <h2>Emisi&oacute;n por lotes (por tipo de plan)</h2>
        <form action="recibo_emit_batch.php" method="POST">
            <input type="hidden" name="tipo" value="1">
            <table id="tabla_form" width="28%">
                <tr>
                    <td><label for="tipo_plan">Tipo de plan</label></td>
                    <td>
                        <select name="tipo_plan">
                        <option value="1">Mensual</option>
                        <option value="2">Bimestral</option>
                        <option value="3">Trimestral</option>
                        <option value="4">Cuatrimestral</option>
                        <option value="6">Semestral</option>
                        <option value="12">Anual</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label for="monto">Monto</label></td>
                    <td><input type="text" name="monto" id="monto"></td>
                </tr>
                <tr>
                    <td align="right">&nbsp;</td>
                    <td><input type="submit" value="Generar "></td>
                </tr>
            </table>
        </form>


        <br><br><br>
        <h2>Emisi&oacute;n por lotes (por categor&iacute;a)</h2>
        <form action="recibo_emit_batch.php" method="POST">
            <input type="hidden" name="tipo" value="2">
            <table id="tabla_form" width="28%">
                <tr>
                    <td><label for="categoria">Tipo de plan</label></td>
                    <td>
                        <select name="categoria">
                        <?php
                            $link = $FUNCIONES->conectar("club");
                            $res = $link->query("select * from categorias");

                            while ($rows = $res->fetch_array())
                            {
                                echo "<option value='";
                                echo $rows["id"];
                                echo "'>".$rows["nombre"];
                                echo "</option>";
                            }
                        ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label for="monto">Monto</label></td>
                    <td><input type="text" name="monto" id="monto"></td>
                </tr>
                <tr>
                    <td align="right">&nbsp;</td>
                    <td><input type="submit" value="Generar "></td>
                </tr>
            </table>
        </form>

    </td>
    </tr>
</table>
</body>
</html>
