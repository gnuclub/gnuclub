<?php
/*
 * socio_lst.php
 *
 * Copyright 2015 Abel Sendón <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

?>

<?php require("motor.php") ?>
<!DOCTYPE html>
<html>
<head>
    <title>GNUClub/Socio/Listar</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="generator" content="gvim 7.3" />
    <link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" href="estiloSocio.css">
</head>
<body>

<?php $WEB->mainMenu() ?>
<table width="90%" align="left" id="tab_body">
<tr><td valign="top" align="center">

<h1>Socio/Listar</h1>
<br>
<form action="socio_lst.php" method="POST">
<input type="hidden" name="db3930b221800c68" id="db3930b221800c68" value="1">
<table id="tabla_form">
    <tr>
    <td><label for="tipo">Ordenar por</label></td>
    <td><select name="tipo" id="tipo">
        <option value="nombre">Nombre</option>
        <option value="apellido">Apellido</option>
        <option value="dni">Cédula</option>
        <option value="id">ID</option>
    </select></td>
    <tr>
        <td><label for="cant">Cantidad</label></td>
        <td><input type="text" name="cant" id="cant"></td>
    </tr>
    <tr>
        <td align="right"><input type="submit" value="Listar"></td>
        <td><input type="reset" value="Resetear"></td>
    </tr>
</table>
</form>

<?php 
$hab = $_POST["db3930b221800c68"];

if($hab == 1) {
    $tipo = $_POST["tipo"];
    $cant = $_POST["cant"];
    
    if($cant == "") 
    { 
        $cant = 10; 
    }
        
    $sql0 = "select id, nombre, apellido, dni, estado from socio order by $tipo limit 0, $cant";

    $l = $FUNCIONES->conectar("club");
    $res = $l->query($sql0);
    
    echo "<br><table width=\"80%\" cellspacing=0 id=\"tabla_form\">";
    echo "<tr bgcolor=\"#BBB\">";
    echo "<td><b>Id</b></td>";
    echo "<td><b>Nombre</b></td>";
    echo "<td><b>Apellido</b></td>";
    echo "<td><b>C&eacute;dula</b></td>";
    echo "<td align=\"center\"><b>Acciones</b></td>";
    echo "</tr>";
    
    $bgDown="";
    while($rows = $res->fetch_array()) {
        if($rows["estado"] == "no") {
           $bgDown="style=\"background:#F44\"";
        }
        else {
            $bgDown="";
        }

        echo "<tr id=\"line_list\" $bgDown>";
        echo "<td>&nbsp;", $rows["id"], "</td>";
        echo "<td>", $rows["nombre"], "</td>";
        echo "<td>", $rows["apellido"], "</td>";
        echo "<td>", $rows["dni"], "</td>";
        echo "<td width=\"25%\" align=\"center\">";
        $i = $rows["id"];
        $r="fbd39ff8da6d47fe19e560b268815112a6d47fe19ezrf=$i";
        echo "<a href=\"socio_ver.php?$r\">Informaci&oacute;n</a>&nbsp;|&nbsp;";
        echo "<a href=\"recibo.php?$r\">Recibo</a>";
        echo "<br>";
        echo "<a href=\"socio_mod.php?$r&8141807c2aaf8b734619e603d042780a07c2aaf8b734=lst\">Modificar</a>&nbsp;|&nbsp;";
        echo "<a href=\"socio_del.php?$r&8141807c2aaf8b734619e603d042780a07c2aaf8b734=lst\">Eliminar</a>";
        echo "&nbsp;</td>";
        echo "</tr>";
    }
    
    echo "</table>";
    $l->close();
}
?>

</td></tr>
</table>
</body>
</html>
