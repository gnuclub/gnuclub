<?php
/*
 * actividad.php
 *
 * Copyright 2015 Abel Sendón <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


require("motor.php");
require("config.php");
?>

<!DOCTYPE html>
<html>
<head>
    <title>GNUClub/Actividad</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="generator" content="Geany 1.23.1" />
    <link rel="stylesheet" href="estilo.css">
</head>
<body>

<?php
$WEB->mainMenu();
$club_ = $CLUB;
?>

<table width="90%"  align="left" id="tab_body">
    <tr><td align="center" valign="top">

    <h1>Actividad</h1>
        <br><hr>

        <h2>Historial de usuario</h2>
        <table align="center" width="60%">
        <tr>
            <td align="right">
                <form action="historial_usuario.php" method="POST">
                <label for="ids">ID de socio</label>&nbsp;
            </td>
            <td>
                <input type="text" name="ids" id="ids">
                <input type="submit" value="Ver">
                </form>
            </td>
        </tr>
        <tr>
            <td align="right">
                <form action="historial_usuario.php" method="POST">
                <label for="dni">Nº de c&eacute;dula/DNI</label>&nbsp;
            </td>
            <td>
                <input type="text" name="dni" id="dni">
                <input type="submit" value="Ver">
                </form>
            </td>
        </tr>
        </table>

        <br><hr>

        <h2>Plan de abono</h2>
        <table align="center" width="60%">
        <tr>
            <td align="right">
                <form action="plan_ver.php" method="POST">
                <label for="ids">ID de socio</label>&nbsp;
            </td>
            <td>
                <input type="text" name="ids" id="ids">
                <input type="submit" value="Ver/Modificar">
                </form>
            </td>
        </tr>
        <tr>
            <td align="right">
                <form action="plan_ver.php" method="POST">
                <label for="dni">Nº de c&eacutedula/DNI</label>&nbsp;
            </td>
            <td>
                <input type="text" name="dni" id="dni">
                <input type="submit" value="Ver/Modificar">
                </form>
            </td>
        </tr>
        </table>

        <br><hr>

        <h2>Historial de pagos</h2>
        <table align="center" width="60%">
        <tr>
            <td align="right">
                <form action="historial_pagos.php" method="POST">
                <label for="ids">ID de socio</label>&nbsp;
            </td>
            <td>
                <input type="text" name="ids" id="ids">
                <input type="submit" value="Ver">
            </td>
        </tr>
        <tr>
            <td align="right">
                <form action="historial_pagos.php" method="POST">
                <label for="dni">Nº de c&eacutedula/DNI</label>&nbsp;
            </td>
            <td>
                <input type="text" name="dni" id="dni">
                <input type="submit" value="Ver">
            </td>
        </tr>
        </table>

        <br><hr><br><br>

        Puede especificarse el Nº de c&eacute;dula/DNI o el ID de socio, no es necesario rellenar ambos.
</body>
</html>
