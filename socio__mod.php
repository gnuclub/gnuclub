<?php
/*
 * socio__mod.php
 *
 * Copyright 2015 Abel Sendón <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

require("motor.php");
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <title>GNUClub/Socio/Modificar</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="generator" content="gvim 7.3" />
    <link rel="stylesheet" href="estilo.css">
</head>

<body>

<?php $WEB->mainMenu() ?>
<table width="90%" align="left" id="tab_body">
<tr><td valign="top" align="center">

    <h1>Socio/Modificar</h1>
    <a href="socio.php"><input type="button" value="Volver al menú socio"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="socio_add.php"><input type="button" value="        Atrás       "></a>
    <br>

<?php
$confirmar = $_POST["confirmado"];
if($confirmar == 1) {
    $i = $UTILS->clean($_POST["idr"]);
    $data = array(
        "idr" => $i,
        "nom" => $UTILS->clean($_POST["nombre"]),
        "ape" => $UTILS->clean($_POST["apellido"]),
        "dni" => $UTILS->clean($_POST["dni"]),
        "nc2" => $UTILS->clean($_POST["nacionalidad"]),
        "est" => $UTILS->clean($_POST["estado_civil"]),
        "cal" => $UTILS->clean($_POST["dom_calle"]),
        "num" => $UTILS->clean($_POST["dom_numero"]),
        "cp"  => $UTILS->clean($_POST["dom_cp"]),
        "loc" => $UTILS->clean($_POST["dom_localidad"]),
        "pro" => $UTILS->clean($_POST["dom_provincia"]),
        "te1" => $UTILS->clean($_POST["telefono1"]),
        "te2" => $UTILS->clean($_POST["telefono2"]),
        "eml" => $UTILS->clean($_POST["email"]),
        "dto" => $UTILS->clean($_POST["depto"]),
        "cat" => $UTILS->clean($_POST["cat"]),
        "obs" => $UTILS->clean($_POST["obs"])
    );

    $foto = $_FILES["foto"];
    if($foto != null) {
    $img = "./fotos/foto-id-".$data['idr'].".jpg";
        if (is_uploaded_file($_FILES["foto"]["tmp_name"])) {
             copy($_FILES["foto"]["tmp_name"], "$img");
        }
    }

    $Socio->modificar($data);
    echo "<br><h3>Los datos del socio han sido modificados.</h3>";

    $data2 = array("id" => $i, "fecha" => $UTILS->now(1), "tipo" => 5);
    $Socio->add_historial($data2);
}
?>

</td></tr>
</table>
</body>
</html>
