<?php
/*
 * index.php
 *
 * Copyright 2015 Abel Sendón <abelnicolas1976@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

?>

<?php 
require("motor.php");
require("config.php");
?>

<!DOCTYPE html>
<html>
<head>
    <title>GNUClub</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="generator" content="Geany 1.23.1" />
    <link rel="stylesheet" href="estilo.css">
</head>
<body>

<?php 
$WEB->mainMenu();
?>

<table width="90%"  align="left" id="tab_body">
    <tr>
        <td align="center" valign="top">
        <br><br>
        <h1 style="font-size: 96px"><?php echo $CLUB ?></h1>
        <img src="fotos/logo.jpg" width="533" height="400">
        <br><br><br><br><br><br><br><br><br><br>
        
        <br><h2>Administrado con GNUClub</h2>

    </td>
    </tr>
</table> 

</body>
</html>
